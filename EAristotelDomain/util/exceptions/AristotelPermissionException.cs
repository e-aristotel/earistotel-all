﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDomain.util.exceptions
{
    public class AristotelPermissionException : AristotelBaseException
    {
        public AristotelPermissionException(string permission) : base("Missing permission: " + permission)
        {
        }
    }
}