﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDomain.util.exceptions
{
    public class AristotelBaseException : Exception
    {
        public AristotelBaseException(string message) : base(message)
        {
        }

        public AristotelBaseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}