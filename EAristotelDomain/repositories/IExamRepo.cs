﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exam;

namespace EAristotelDomain.repositories
{
    public interface IExamRepo: INamedRepository<Exam>
    {
        /*
        add custom methods
        */
    }
}
