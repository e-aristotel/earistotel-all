﻿using EAristotelDomain.model.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDomain.repositories
{
    public interface IExerciseSolutionRepo : IRepository<ExerciseSolution>
    {
        /*
        add custom methods
        */
    }
}
