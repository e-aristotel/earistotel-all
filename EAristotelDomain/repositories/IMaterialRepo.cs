﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.material;

namespace EAristotelDomain.repositories
{
    public interface IMaterialRepo: INamedRepository<Material>
    {
        /*
        add custom methods
        */
    }
}
