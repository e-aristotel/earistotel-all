﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using EAristotelDomain.model.repository;

namespace EAristotelDomain.repositories
{
    public interface ILeRepositoryRepo : INamedRepository<LeRepository>
    {
        /*
        add custom methods
        */
    }
}
