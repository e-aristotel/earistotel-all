﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.accounts;

namespace EAristotelDomain.repositories
{
    public interface IAdminRepo: IRepository<Admin>
    {
        /*
        add custom methods
        */
    }
}
