﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using EAristotelDomain.repositories;

namespace EAristotelDomain.repositories
{
    public interface INamedRepository<T> : IRepository<T> where T:IHasName
    {
        /// <summary>
        /// Returns all the T entities where T.Name contains 
        /// the given query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IList<T> SearchByName(string query);
    }
}
