﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exercise;

namespace EAristotelDomain.repositories
{
    public interface IExerciseRepo : INamedRepository<Exercise>
    {
        /*
        add custom methods
        */
    }
}