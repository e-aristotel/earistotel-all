﻿namespace EAristotelDomain.model
{
    public abstract class Solution: EntityBase<int>
    {
        public virtual string Result { get; set; }
    }
}
