﻿using System.Collections.Generic;
using EAristotelDomain.model.exam;
using EAristotelDomain.model.exercise;
using EAristotelDomain.model.material;

namespace EAristotelDomain.model.repository
{
    public class LeRepository: EntityBase<int>, IHasName
    {
        public virtual string Name { get; set; }
        public virtual LeRepository ParentRepository { get; set; }
        public virtual IList<LeRepository> ChildrenRepositories { get; set; } = new List<LeRepository>();
        public virtual IList<Exercise> Exercises { get; set; } = new List<Exercise>();
        public virtual IList<Exam> Exams { get; set; } = new List<Exam>();
        public virtual IList<Material> Materials { get; set; } = new List<Material>();
    }
}