﻿namespace EAristotelDomain.model.exercise
{
    public class ExerciseSolution: Solution
    {
        public virtual string StepByStep { get; set; }
        public virtual Exercise Exercise { get; set; }
    }
}
