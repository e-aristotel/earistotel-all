﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.repository;

namespace EAristotelDomain.model.exercise
{
    public class Exercise: EntityBase<int>, IHasName
    {
        public virtual string Name { get; set; }
        public virtual string Content { get; set; }
        public virtual ExerciseSolution BestSolution { get; set; }
        public virtual IList<ExerciseSolution> Solutions { get; set; } = new List<ExerciseSolution>();
        public virtual IList<string> Tags { get; set; } = new List<string>();
        public virtual LeRepository Repository { get; set; }

        public virtual void AddSolution(ExerciseSolution exerciseSolution)
        {
            Solutions.Add(exerciseSolution);
            exerciseSolution.Exercise = this;
        }
    }
}