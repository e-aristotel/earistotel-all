﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDomain.model.exercise
{
    [Obsolete("do not use", true)]
    public class Tag: EntityBase<int>
    {
        public virtual string TagString { get; set; }
        //public virtual IList<Exercise> Exercises { get; set; } = new List<Exercise>();
    }
}