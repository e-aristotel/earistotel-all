﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.repository;

namespace EAristotelDomain.model.material
{
    public class Material: EntityBase<int>, IHasName
    {
        public virtual string Name { get; set; }
        public virtual string Author { get; set; }
        public virtual string Content { get; set; }
        public virtual LeRepository Repository { get; set; }
    }
}