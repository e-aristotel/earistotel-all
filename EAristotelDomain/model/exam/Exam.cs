﻿using System.Collections.Generic;
using EAristotelDomain.model.repository;

namespace EAristotelDomain.model.exam
{
    public class Exam: EntityBase<int>, IHasName
    {
        public virtual string Name { get; set; }
        public virtual IList<ExamTask> ExamTasks { get; set; } = new List<ExamTask>();
        public virtual LeRepository Repository { get; set; }

        public virtual void AddTask(ExamTask examTask)
        {
            ExamTasks.Add(examTask);
            examTask.Exam = this;
        }

        public virtual void RemoveTask(ExamTask task)
        {
            ExamTasks.Remove(task);
            task.Exam = null;
        }

        public virtual ExamEvaluationModel Evaluate(ExamEvaluationModel submittedExam)
        {
            submittedExam.Evaluate(ExamTasks);
            return submittedExam;
        }
    }

    public class ExamEvaluationModel
    {
        public int Id { get; set; }
        public List<ExamEvaluationAnswerModel> Answers { get; set; }
        public double ScorePerc { get; set; }

        public void Evaluate(IList<ExamTask> examTasks)
        {
            int numCorrect = 0;
            foreach (var task in examTasks)
            {
                ExamEvaluationAnswerModel answer = Answers.Find(a=>a.QuestionId.Equals(task.Id));
                if (answer == null)
                {
                    continue;
                }
                else
                {
                    answer.CorrectAnswerId = task.CorrectSolution.Id;
                    if (answer.IsCorrect())
                    {
                        numCorrect++;
                    }
                }
            }

            ScorePerc = ((double) numCorrect) / examTasks.Count;
        }
    }

    public class ExamEvaluationAnswerModel
    {
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public int? CorrectAnswerId { get; set; }

        public bool IsCorrect()
        {
            return CorrectAnswerId.HasValue && CorrectAnswerId.Equals(AnswerId);
        }
    }
}