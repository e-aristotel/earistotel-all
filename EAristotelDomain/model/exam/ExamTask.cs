﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDomain.model.exam
{
    public class ExamTask:EntityBase<int>
    {
        public virtual string Content { get; set; }
        public virtual IList<ExamTaskSolution> OfferedSolutions { get; set; } = new List<ExamTaskSolution>();
        public virtual ExamTaskSolution CorrectSolution { get; set; }
        public virtual Exam Exam { get; set; }
    }
}