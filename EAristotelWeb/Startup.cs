﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EAristotelWeb.Startup))]
namespace EAristotelWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
