﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAristotelDomain.model.exam;

namespace EAristotelWeb.Models
{
    public class SimpleExamModel
    {
        public string Name { get; set; }
        public int? Id { get; set; }


        public SimpleExamModel(Exam exam)
        {
            if (exam != null)
            {
                Id = exam.Id;
                Name = exam.Name;
            }
        }
    }


}