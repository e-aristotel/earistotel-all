﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAristotelBLL.services;
using EAristotelDomain.model.repository;
using WebGrease.Css.Extensions;

namespace EAristotelWeb.Models
{
    public class RepositoryDetailsModel
    {
        public RepositoryDetailsModel(LeRepository repository)
        {
            Id = repository.Id;
            Name = repository.Name;
            ParentRepository = new SimpleRepositoryModel(repository.ParentRepository);
            repository.ChildrenRepositories.ForEach(
                repo => ChildrenRepositories.Add(new SimpleRepositoryModel(repo)));
            repository.Exercises.ForEach(exercise => Exercises.Add(new SimpleExerciseModel(exercise)));
            repository.Exams.ForEach(exam => Exams.Add(new SimpleExamModel(exam)));
            repository.Materials.ForEach(material => Materials.Add(new SimpleMaterialModel(material)));
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public SimpleRepositoryModel ParentRepository { get; set; }
        public List<SimpleRepositoryModel> ChildrenRepositories { get; set; } = new List<SimpleRepositoryModel>();
        public List<SimpleExerciseModel> Exercises { get; set; } = new List<SimpleExerciseModel>();
        public List<SimpleExamModel> Exams { get; set; } = new List<SimpleExamModel>();
        public List<SimpleMaterialModel> Materials { get; set; } = new List<SimpleMaterialModel>();
    }

    public class SimpleRepositoryModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public SimpleRepositoryModel(LeRepository repo)
        {
            if (repo != null)
            {
                Id = repo.Id;
                Name = repo.Name;
            }
        }
    }
}