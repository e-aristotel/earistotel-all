﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAristotelDomain.model.material;

namespace EAristotelWeb.Models
{
    public class SimpleMaterialModel
    {
        public string Name { get; set; }
        public int? Id { get; set; }


        public SimpleMaterialModel(Material material)
        {
            if (material != null)
            {
                Id = material.Id;
                Name = material.Name;
            }
        }
    }
}