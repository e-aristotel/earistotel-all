﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EAristotelDomain.model.exercise;

namespace EAristotelWeb.Models
{
    public class SimpleExerciseModel
    {
        public string Name { get; set; }
        public int? Id { get; set; }
        public string Content { get; set; }

        public SimpleExerciseModel(Exercise exercise)
        {
            if (exercise != null)
            {
                Id = exercise.Id;
                Name = exercise.Name;
                Content = exercise.Content;
            }
        }
    }
}