﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAristotelBLL;

namespace EAristotelWeb.Controllers
{
    public class ControllerBLL : Controller
    {
        private static BllServiceProvider _provider;
        protected BllServiceProvider Provider => _provider ?? (_provider = new BllServiceProvider());
    }
}