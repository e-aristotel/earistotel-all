﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDomain.model.exam;
using EAristotelWeb.Models;
using WebGrease.Css.Extensions;

namespace EAristotelWeb.Controllers
{
    public class ExamController : ControllerBLL
    {
        // GET: Exam
        public ActionResult Index()
        {
            return View(Provider.ExamService.GetAllExams());
        }

        // GET: Exam/Details/5
        public ActionResult TakeExam(int id)
        {
            Exam e;
            //load the associations needed for this View
            using (BllContext c = BllContextFactory.CreateContext())
            {
                e = loadExamViewModel(id, c);
            }

            ViewBag.Results = false;
            return View(e);
        }

        private Exam loadExamViewModel(int id, BllContext c)
        {
            Exam e = Provider.ExamService.GetExam(id, c);

            e.Repository.Id = e.Repository.Id;
            e.Repository.Name = e.Repository.Name;

            e.ExamTasks = e.ExamTasks.ToList();
            e.ExamTasks.ForEach(et =>
            {
                et.Content = et.Content;

                et.OfferedSolutions.ForEach(os =>
                {
                    os.Id = os.Id;
                    os.Result = os.Result;
                });

            });

            return e;
        }

        // POST: 
        [HttpPost]
        public ActionResult TakeExam(ExamEvaluationModel submittedExam)
        {
            Exam e;
            ExamEvaluationModel model;
            //load the associations needed for this View
            using (BllContext c = BllContextFactory.CreateContext())
            {
                model = Provider.ExamService.EvaluateExam(submittedExam, c);
                e = loadExamViewModel(model.Id, c);
            }

            ViewBag.EvaluationModel = model;
            ViewBag.Results = true;
            return View(e);
        } 
    }


}