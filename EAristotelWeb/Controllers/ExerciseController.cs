﻿using EAristotelBLL;
using EAristotelBLL.factories;
using EAristotelBLL.services;
using EAristotelDomain.model.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace EAristotelWeb.Controllers
{
    public class ExerciseController : ControllerBLL
    {
        // GET: Exercise
        public ActionResult Index()
        {
            return View(Provider.ExerciseService.GetAllExercises());
        }

        // GET: Exercise/Details/5
        public ActionResult Details(int id)
        {
            Exercise e;
            using (BllContext c = BllContextFactory.CreateContext())
            {
                e = Provider.ExerciseService.GetExercise(id, c);
                ViewBag.RepoId = e.Repository.Id;
                ViewBag.RepoName = e.Repository.Name;
                ViewBag.Solutions = e.Solutions.ToList();
            }
            return View(e);
        }

        // GET: Exercise/Create
        public ActionResult Create()
        {
            ViewBag.Repos = new SelectList(Provider.LeRepositoryService.GetRepositories(), "Id", "Name");
            return View();
        }

        // POST: Exercise/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                string name = collection.Get("Name");
                string content = collection.Get("Content");
                int repositoryId = int.Parse(collection.Get("Repository"));
                string tags = collection.Get("Tag");

                Exercise exercise = ExerciseFactory.CreateExercise(name, content);
                if (!string.IsNullOrWhiteSpace(tags))
                {
                    Regex.Replace(tags, @"\s+", "");
                    exercise.Tags = tags.Split(';').ToList();
                }

                Provider.ExerciseService.CreateExercise(repositoryId, exercise);

                return RedirectToAction("Details", new { Id = exercise.Id });
            }
            catch
            {   
                ViewBag.Repos = new SelectList(Provider.LeRepositoryService.GetRepositories(), "Id", "Name");
                ViewBag.Error = "Error creating new exercise";
                return View();
            }
        }

        // GET: Exercise/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Provider.ExerciseService.GetExercise(id));
        }

        // POST: Exercise/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                string name = collection.Get("Name");
                string content = collection.Get("Content");

                Exercise exercise = Provider.ExerciseService.GetExercise(id);
                exercise.Name = name;
                exercise.Content = content;

                Provider.ExerciseService.UpdateExercise(exercise);

                return RedirectToAction("Details", new { Id = exercise.Id });
            }
            catch
            {
                ViewBag.Repos = new SelectList(Provider.LeRepositoryService.GetRepositories(), "Id", "Name");
                ViewBag.Error = "Error creating new exercise";
                return View();
            }
        }

        // GET: Exercise/CreateSolution
        public ActionResult CreateSolution(int? eId)
        {
            ViewBag.Exercises = new SelectList(Provider.ExerciseService.GetAllExercises(), "Id", "Name", eId);
            return View();
        }

        // POST: Exercise/CreateSolution
        [HttpPost]
        public ActionResult CreateSolution(FormCollection collection)
        {
            int exerciseId = int.Parse(collection.Get("Exercise"));
            string result = collection.Get("Result");
            string stepByStep = collection.Get("StepByStep");

            Exercise e = Provider.ExerciseService.GetExercise(exerciseId);
            ExerciseSolution es = ExerciseFactory.CreateExerciseSolution(result, stepByStep);
            es.Exercise = e;
            Provider.ExerciseService.AddSolution(exerciseId, es);
            return RedirectToAction("Details", new { Id = e.Id });
        }
    }
}
