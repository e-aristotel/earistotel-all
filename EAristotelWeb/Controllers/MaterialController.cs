﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDomain.model.material;

namespace EAristotelWeb.Controllers
{
    public class MaterialController : ControllerBLL
    {
        // GET: Exercise
        public ActionResult Index()
        {
            return View(Provider.MaterialService.GetAllMaterials());
        }

        // GET: Exercise/Details/5
        public ActionResult Details(int id)
        {
            Material m;
            using (BllContext c = BllContextFactory.CreateContext())
            {
                m = Provider.MaterialService.GetMaterial(id, c);
                ViewBag.RepoId = m.Repository.Id;
                ViewBag.RepoName = m.Repository.Name;
            }
            return View(m);
        }

    }
}