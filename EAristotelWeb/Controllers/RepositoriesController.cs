﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDomain.model.repository;
using EAristotelWeb.Models;

namespace EAristotelWeb.Controllers
{
    public class RepositoriesController : ControllerBLL
    {
        // GET: Repositories
        public ActionResult Index()
        {
            return View(Provider.LeRepositoryService.GetTopLevelRepositories());
        }

        // GET: Repositories/Details/5
        public ActionResult Details(int id)
        {
            RepositoryDetailsModel repo;
            using (BllContext context = BllContextFactory.CreateContext())
            {
                repo= new RepositoryDetailsModel(Provider.LeRepositoryService.GetRepository(id, context));
            }

            ViewBag.Title = repo.Name;
            return View(repo);
        }

        // GET: Repositories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Repositories/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Repositories/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Repositories/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Repositories/Delete/5
        public ActionResult Delete(int id)
        {
            //admin only
            //Provider.LeRepositoryService.RemoveRepository(id);

            return RedirectToAction("Index");
        }

        // POST: Repositories/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Search(string query)
        {
            RepositoryDetailsModel repo;
            using (var context=BllContextFactory.CreateContext())
            {
                repo = new RepositoryDetailsModel(Provider.LeRepositoryService.SearchEverythingForRepo(query, context));
            }

            ViewBag.Title = "Search results for '" + query + "'";
            return View("Details", repo);
        }
    }
}
