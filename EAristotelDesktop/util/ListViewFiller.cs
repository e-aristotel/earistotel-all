﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model;

namespace EAristotelDesktop.util
{
    internal static class ListViewFiller
    {
        public static void RepopulateListView<T>(IEnumerable<T> entities, Func<T, string> textFunc,
            ListView listViewToPopulate) where T : EntityBase<int>
        {
            listViewToPopulate.Items.Clear();
            foreach (var entity in entities)
            {
                listViewToPopulate.Items.Add(new ListViewItem
                {
                    Tag = entity.Id,
                    Text = textFunc(entity)
                });
            }
        }
    }
}
