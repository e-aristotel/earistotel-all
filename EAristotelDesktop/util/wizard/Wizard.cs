﻿using System.Collections.Generic;

namespace EAristotelDesktop.util.wizard
{
    public sealed class Wizard
    {
        private readonly IWizardForm _firstWizardForm;
        private readonly IList<IWizardForm> _wizardForms = new List<IWizardForm>();

        public delegate void OnCloseEventHandler();

        public event OnCloseEventHandler OnClose;

        public Wizard(IWizardForm firstWizardForm)
        {
            _firstWizardForm = firstWizardForm;
            CollectWizardForms(firstWizardForm);
            InitWizardFormsBehaviour();
        }

        private void InitWizardFormsBehaviour()
        {
            foreach (var wizardForm in _wizardForms)
            {
                wizardForm.OnPrevious += () =>
                {
                    wizardForm.Hide();
                    wizardForm.Previous?.Show();
                };
                wizardForm.OnNext += next =>
                {
                    wizardForm.Hide();
                    next?.Show();
                };
                wizardForm.OnCancel += CloseAll;
                wizardForm.OnFinish += CloseAll;
            }
        }

        private void CollectWizardForms(IWizardForm wizardForm)
        {
            _wizardForms.Add(wizardForm);

            if (wizardForm.Next != null)
            {
                foreach (var nextForm in wizardForm.Next)
                {
                    CollectWizardForms(nextForm);
                }
            }
        }

        public void Start()
        {
            _firstWizardForm.Show();
        }

        private void CloseAll()
        {
            foreach (var wizardForm in _wizardForms)
            {
                if (!wizardForm.IsClosed)
                {
                    wizardForm.Close();
                }
            }
            OnClose?.Invoke();
        }
    }
}