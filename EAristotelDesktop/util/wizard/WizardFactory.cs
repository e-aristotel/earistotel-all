﻿using EAristotelBLL.services;
using EAristotelDesktop.forms.add_item;
using EAristotelDesktop.forms.add_item.exam;

namespace EAristotelDesktop.util.wizard
{
    public class WizardFactory
    {
        public static Wizard CreateAddItemToRepositoryWizard(IWindowsFormsFactory windowsFormsFactory,
            IBllServiceProvider bllServiceProvider, int repositoryId, string token)
        {
            IWizardForm chooseItemTypeForm =
                new ChooseItemTypeForm(new AddMaterialForm(bllServiceProvider, repositoryId, token),
                    new AddExerciseForm(windowsFormsFactory, bllServiceProvider, repositoryId),
                    new AddExamForm(windowsFormsFactory, bllServiceProvider, repositoryId, token));
            return new Wizard(chooseItemTypeForm);
        }
    }
}