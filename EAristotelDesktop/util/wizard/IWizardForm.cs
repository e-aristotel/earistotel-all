﻿using System.Collections.Generic;

namespace EAristotelDesktop.util.wizard
{
    public delegate void WizardSimpleDelegate();

    public delegate void WizardNextFormDelegate(IWizardForm nextWizardForm);

    public interface IInWizardForm<in TIn>: IWizardForm
    {
        TIn InputData { set; }
    }

    public interface IOutWizardForm<out TOut> : IWizardForm
    {
        TOut OutputData { get; }
    }
    public interface IWizardForm
    {
        IWizardForm Previous { get; set; }
        IEnumerable<IWizardForm> Next { get; }

        event WizardSimpleDelegate OnFinish;
        event WizardNextFormDelegate OnNext;
        event WizardSimpleDelegate OnPrevious;
        event WizardSimpleDelegate OnCancel;
        void Show();
        void Hide();
        void Close();
        bool IsClosed { get; }

    }
}
