﻿namespace EAristotelDesktop.util.wrappers
{
    public class RepositoryInfoWrapper
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}