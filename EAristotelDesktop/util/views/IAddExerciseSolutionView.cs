﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDesktop.util.views
{
    public interface IAddExerciseSolutionView: IDialog
    {
        string Result { get; }
        string StepByStep { get; }
    }
}