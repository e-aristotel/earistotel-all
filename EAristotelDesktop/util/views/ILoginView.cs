﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDesktop.util.views
{
    public interface ILoginView: IDialog
    {
        string Username { get; }
        string Password { get; }
    }
}
