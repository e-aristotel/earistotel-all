﻿namespace EAristotelDesktop.util.views
{
    public interface ICreateRepositoryView: IDialog
    {
        string RepositoryName { get; }
        int? ParentRepositoryId { get; }
    }
}
