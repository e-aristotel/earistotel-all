﻿using System.Windows.Forms;

namespace EAristotelDesktop.util.views
{
    public interface IDialog
    {
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window owner);
    }
}
