﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exam;

namespace EAristotelDesktop.util.views
{
    public interface IAddExamTaskView: IDialog
    {
        string Content { get; }
        List<ExamTaskSolution> Solutions { get; }
        ExamTaskSolution CorrectSolution { get; }
    }
}
