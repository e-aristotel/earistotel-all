﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDesktop.util.views;
using EAristotelDesktop.util.wrappers;

namespace EAristotelDesktop.util
{
    public interface IWindowsFormsFactory
    {
        ICreateRepositoryView CreateRepositoryView(List<RepositoryInfoWrapper> repositoryList);
        IAddExerciseSolutionView CreateAddExerciseSolutionView();
        ILoginView CreateLoginView();
        IAddExamTaskView CreateAddExamTaskView();
    }
}
