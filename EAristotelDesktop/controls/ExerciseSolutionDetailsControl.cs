﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model.exercise;

namespace EAristotelDesktop.controls
{
    public partial class ExerciseSolutionDetailsControl : UserControl
    {
        public ExerciseSolutionDetailsControl()
        {
            InitializeComponent();
        }

        public ExerciseSolution ExerciseSolution
        {
            set
            {
                if (value != null)
                {
                    solutionRichTextBox.Text = value.Result;
                    stepByStepRichTextBox.Text = value.StepByStep;
                }
                else
                {
                    solutionRichTextBox.Clear();
                    stepByStepRichTextBox.Clear();
                }
            }
        }

        public string SolutionResult => solutionRichTextBox.Text;
        public string SolutionStepByStep => stepByStepRichTextBox.Text;

        private void stepByStepRichTextBox_TextChanged(object sender, EventArgs e)
        {
            stepByStepWebBrowser.DocumentText = ScriptUtil.MathJaxHeader + stepByStepRichTextBox.Text;
        }

        private void solutionRichTextBox_TextChanged(object sender, EventArgs e)
        {
            solutionWebBrowser.DocumentText = ScriptUtil.MathJaxHeader + solutionRichTextBox.Text;
        }
    }
}
