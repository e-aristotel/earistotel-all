﻿namespace EAristotelDesktop.controls
{
    partial class ExerciseSolutionDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stepByStepRichTextBox = new System.Windows.Forms.RichTextBox();
            this.solutionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.stepByStepWebBrowser = new System.Windows.Forms.WebBrowser();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.solutionWebBrowser = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // stepByStepRichTextBox
            // 
            this.stepByStepRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepByStepRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.stepByStepRichTextBox.Name = "stepByStepRichTextBox";
            this.stepByStepRichTextBox.Size = new System.Drawing.Size(307, 85);
            this.stepByStepRichTextBox.TabIndex = 11;
            this.stepByStepRichTextBox.Text = "";
            this.stepByStepRichTextBox.TextChanged += new System.EventHandler(this.stepByStepRichTextBox_TextChanged);
            // 
            // solutionRichTextBox
            // 
            this.solutionRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.solutionRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.solutionRichTextBox.Name = "solutionRichTextBox";
            this.solutionRichTextBox.Size = new System.Drawing.Size(307, 50);
            this.solutionRichTextBox.TabIndex = 10;
            this.solutionRichTextBox.Text = "";
            this.solutionRichTextBox.TextChanged += new System.EventHandler(this.solutionRichTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "step by step:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "solution:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(81, 109);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.stepByStepRichTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.stepByStepWebBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(307, 171);
            this.splitContainer1.SplitterDistance = 85;
            this.splitContainer1.TabIndex = 12;
            // 
            // stepByStepWebBrowser
            // 
            this.stepByStepWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepByStepWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.stepByStepWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.stepByStepWebBrowser.Name = "stepByStepWebBrowser";
            this.stepByStepWebBrowser.Size = new System.Drawing.Size(307, 82);
            this.stepByStepWebBrowser.TabIndex = 13;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(81, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.solutionRichTextBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.solutionWebBrowser);
            this.splitContainer2.Size = new System.Drawing.Size(307, 100);
            this.splitContainer2.TabIndex = 13;
            // 
            // solutionWebBrowser
            // 
            this.solutionWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.solutionWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.solutionWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.solutionWebBrowser.Name = "solutionWebBrowser";
            this.solutionWebBrowser.Size = new System.Drawing.Size(307, 46);
            this.solutionWebBrowser.TabIndex = 14;
            // 
            // ExerciseSolutionDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ExerciseSolutionDetailsControl";
            this.Size = new System.Drawing.Size(391, 283);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox stepByStepRichTextBox;
        private System.Windows.Forms.RichTextBox solutionRichTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.WebBrowser stepByStepWebBrowser;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.WebBrowser solutionWebBrowser;
    }
}
