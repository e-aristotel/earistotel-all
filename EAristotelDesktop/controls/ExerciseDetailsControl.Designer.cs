﻿namespace EAristotelDesktop.controls
{
    partial class ExerciseDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exerciseTagsTextBox = new System.Windows.Forms.TextBox();
            this.exerciseContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.exerciseNameTextBox = new System.Windows.Forms.TextBox();
            this.exerciseTagsLabel = new System.Windows.Forms.Label();
            this.exerciseContentLabel = new System.Windows.Forms.Label();
            this.exerciseNameLabel = new System.Windows.Forms.Label();
            this.previewWebBrowser = new System.Windows.Forms.WebBrowser();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // exerciseTagsTextBox
            // 
            this.exerciseTagsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseTagsTextBox.Location = new System.Drawing.Point(65, 34);
            this.exerciseTagsTextBox.Name = "exerciseTagsTextBox";
            this.exerciseTagsTextBox.Size = new System.Drawing.Size(351, 20);
            this.exerciseTagsTextBox.TabIndex = 12;
            // 
            // exerciseContentRichTextBox
            // 
            this.exerciseContentRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exerciseContentRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.exerciseContentRichTextBox.Name = "exerciseContentRichTextBox";
            this.exerciseContentRichTextBox.Size = new System.Drawing.Size(351, 93);
            this.exerciseContentRichTextBox.TabIndex = 11;
            this.exerciseContentRichTextBox.Text = "";
            this.exerciseContentRichTextBox.TextChanged += new System.EventHandler(this.exerciseContentRichTextBox_TextChanged);
            // 
            // exerciseNameTextBox
            // 
            this.exerciseNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseNameTextBox.Location = new System.Drawing.Point(65, 3);
            this.exerciseNameTextBox.Name = "exerciseNameTextBox";
            this.exerciseNameTextBox.Size = new System.Drawing.Size(351, 20);
            this.exerciseNameTextBox.TabIndex = 10;
            // 
            // exerciseTagsLabel
            // 
            this.exerciseTagsLabel.AutoSize = true;
            this.exerciseTagsLabel.Location = new System.Drawing.Point(3, 37);
            this.exerciseTagsLabel.Name = "exerciseTagsLabel";
            this.exerciseTagsLabel.Size = new System.Drawing.Size(56, 13);
            this.exerciseTagsLabel.TabIndex = 9;
            this.exerciseTagsLabel.Text = "tags (csv):";
            // 
            // exerciseContentLabel
            // 
            this.exerciseContentLabel.AutoSize = true;
            this.exerciseContentLabel.Location = new System.Drawing.Point(3, 67);
            this.exerciseContentLabel.Name = "exerciseContentLabel";
            this.exerciseContentLabel.Size = new System.Drawing.Size(46, 13);
            this.exerciseContentLabel.TabIndex = 8;
            this.exerciseContentLabel.Text = "content:";
            // 
            // exerciseNameLabel
            // 
            this.exerciseNameLabel.AutoSize = true;
            this.exerciseNameLabel.Location = new System.Drawing.Point(3, 6);
            this.exerciseNameLabel.Name = "exerciseNameLabel";
            this.exerciseNameLabel.Size = new System.Drawing.Size(36, 13);
            this.exerciseNameLabel.TabIndex = 7;
            this.exerciseNameLabel.Text = "name:";
            // 
            // previewWebBrowser
            // 
            this.previewWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.previewWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.previewWebBrowser.Name = "previewWebBrowser";
            this.previewWebBrowser.Size = new System.Drawing.Size(351, 89);
            this.previewWebBrowser.TabIndex = 14;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(65, 67);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.exerciseContentRichTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.previewWebBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(351, 186);
            this.splitContainer1.SplitterDistance = 93;
            this.splitContainer1.TabIndex = 15;
            // 
            // ExerciseDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.exerciseTagsTextBox);
            this.Controls.Add(this.exerciseNameTextBox);
            this.Controls.Add(this.exerciseTagsLabel);
            this.Controls.Add(this.exerciseContentLabel);
            this.Controls.Add(this.exerciseNameLabel);
            this.Name = "ExerciseDetailsControl";
            this.Size = new System.Drawing.Size(419, 256);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox exerciseTagsTextBox;
        private System.Windows.Forms.RichTextBox exerciseContentRichTextBox;
        private System.Windows.Forms.TextBox exerciseNameTextBox;
        private System.Windows.Forms.Label exerciseTagsLabel;
        private System.Windows.Forms.Label exerciseContentLabel;
        private System.Windows.Forms.Label exerciseNameLabel;
        private System.Windows.Forms.WebBrowser previewWebBrowser;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}
