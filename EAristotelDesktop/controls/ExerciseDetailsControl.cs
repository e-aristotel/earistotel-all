﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model.exercise;

namespace EAristotelDesktop.controls
{
    public partial class ExerciseDetailsControl : UserControl
    {
        public ExerciseDetailsControl()
        {
            InitializeComponent();
        }

        public Exercise Exercise
        {
            set
            {
                if (value != null)
                {
                    exerciseNameTextBox.Text = value.Name;
                    exerciseTagsTextBox.Text = string.Join(", ", value.Tags);
                    exerciseContentRichTextBox.Text = value.Content;
                }
                else
                {
                    exerciseNameTextBox.Clear();
                    exerciseTagsTextBox.Clear();
                    exerciseContentRichTextBox.Clear();
                }
            }
        }

        public string ExerciseName => exerciseNameTextBox.Text.Trim();
        public List<string> Tags => exerciseTagsTextBox.Text.Split(',').Select(t => t.Trim()).ToList();
        public string Content => exerciseContentRichTextBox.Text;

        private void exerciseContentRichTextBox_TextChanged(object sender, EventArgs e)
        {
            previewWebBrowser.DocumentText = ScriptUtil.MathJaxHeader + exerciseContentRichTextBox.Text;
        }
    }
}
