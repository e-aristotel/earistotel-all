﻿namespace EAristotelDesktop.controls
{
    partial class MaterialDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.materialAuthorsTextBox = new System.Windows.Forms.TextBox();
            this.materialNameTextBox = new System.Windows.Forms.TextBox();
            this.materialContentLabel = new System.Windows.Forms.Label();
            this.materialAuthorsLabel = new System.Windows.Forms.Label();
            this.materialNameLabel = new System.Windows.Forms.Label();
            this.previewWebBrowser = new System.Windows.Forms.WebBrowser();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialContentRichTextBox
            // 
            this.materialContentRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialContentRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.materialContentRichTextBox.Name = "materialContentRichTextBox";
            this.materialContentRichTextBox.Size = new System.Drawing.Size(308, 100);
            this.materialContentRichTextBox.TabIndex = 12;
            this.materialContentRichTextBox.Text = "";
            this.materialContentRichTextBox.TextChanged += new System.EventHandler(this.materialContentRichTextBox_TextChanged);
            // 
            // materialAuthorsTextBox
            // 
            this.materialAuthorsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialAuthorsTextBox.Location = new System.Drawing.Point(60, 33);
            this.materialAuthorsTextBox.Name = "materialAuthorsTextBox";
            this.materialAuthorsTextBox.Size = new System.Drawing.Size(308, 20);
            this.materialAuthorsTextBox.TabIndex = 11;
            // 
            // materialNameTextBox
            // 
            this.materialNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialNameTextBox.Location = new System.Drawing.Point(60, 3);
            this.materialNameTextBox.Name = "materialNameTextBox";
            this.materialNameTextBox.Size = new System.Drawing.Size(308, 20);
            this.materialNameTextBox.TabIndex = 10;
            // 
            // materialContentLabel
            // 
            this.materialContentLabel.AutoSize = true;
            this.materialContentLabel.Location = new System.Drawing.Point(3, 65);
            this.materialContentLabel.Name = "materialContentLabel";
            this.materialContentLabel.Size = new System.Drawing.Size(46, 13);
            this.materialContentLabel.TabIndex = 9;
            this.materialContentLabel.Text = "content:";
            // 
            // materialAuthorsLabel
            // 
            this.materialAuthorsLabel.AutoSize = true;
            this.materialAuthorsLabel.Location = new System.Drawing.Point(3, 36);
            this.materialAuthorsLabel.Name = "materialAuthorsLabel";
            this.materialAuthorsLabel.Size = new System.Drawing.Size(51, 13);
            this.materialAuthorsLabel.TabIndex = 8;
            this.materialAuthorsLabel.Text = "author(s):";
            // 
            // materialNameLabel
            // 
            this.materialNameLabel.AutoSize = true;
            this.materialNameLabel.Location = new System.Drawing.Point(3, 6);
            this.materialNameLabel.Name = "materialNameLabel";
            this.materialNameLabel.Size = new System.Drawing.Size(36, 13);
            this.materialNameLabel.TabIndex = 7;
            this.materialNameLabel.Text = "name:";
            // 
            // previewWebBrowser
            // 
            this.previewWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.previewWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.previewWebBrowser.Name = "previewWebBrowser";
            this.previewWebBrowser.Size = new System.Drawing.Size(308, 97);
            this.previewWebBrowser.TabIndex = 14;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(60, 65);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.materialContentRichTextBox);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.previewWebBrowser);
            this.splitContainer2.Size = new System.Drawing.Size(308, 201);
            this.splitContainer2.SplitterDistance = 100;
            this.splitContainer2.TabIndex = 16;
            // 
            // MaterialDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.materialAuthorsTextBox);
            this.Controls.Add(this.materialNameTextBox);
            this.Controls.Add(this.materialContentLabel);
            this.Controls.Add(this.materialAuthorsLabel);
            this.Controls.Add(this.materialNameLabel);
            this.Name = "MaterialDetailsControl";
            this.Size = new System.Drawing.Size(371, 269);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox materialContentRichTextBox;
        private System.Windows.Forms.TextBox materialAuthorsTextBox;
        private System.Windows.Forms.TextBox materialNameTextBox;
        private System.Windows.Forms.Label materialContentLabel;
        private System.Windows.Forms.Label materialAuthorsLabel;
        private System.Windows.Forms.Label materialNameLabel;
        private System.Windows.Forms.WebBrowser previewWebBrowser;
        private System.Windows.Forms.SplitContainer splitContainer2;
    }
}
