﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model.material;

namespace EAristotelDesktop.controls
{
    public partial class MaterialDetailsControl : UserControl
    {
        public MaterialDetailsControl()
        {
            InitializeComponent();
        }

        public Material Material
        {
            set
            {
                if (value != null)
                {
                    materialNameTextBox.Text = value.Name;
                    materialAuthorsTextBox.Text = value.Author;
                    materialContentRichTextBox.Text = value.Content;
                }
                else
                {
                    materialNameTextBox.Clear();
                    materialAuthorsTextBox.Clear();
                    materialContentRichTextBox.Clear();
                }
            }
        }

        public string MaterialName => materialNameTextBox.Text.Trim();
        public string MaterialAuthors => materialAuthorsTextBox.Text;
        public string MaterialContent => materialContentRichTextBox.Text;

        private void materialContentRichTextBox_TextChanged(object sender, EventArgs e)
        {
            previewWebBrowser.DocumentText = ScriptUtil.MathJaxHeader + materialContentRichTextBox.Text;
        }
    }
}