﻿namespace EAristotelDesktop.controls
{
    partial class RepositoryContentControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLabel = new System.Windows.Forms.Label();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.addItem = new System.Windows.Forms.Button();
            this.itemsTabControl = new System.Windows.Forms.TabControl();
            this.materialsTabPage = new System.Windows.Forms.TabPage();
            this.materialsListView = new System.Windows.Forms.ListView();
            this.exercisesTabPage = new System.Windows.Forms.TabPage();
            this.exercisesListView = new System.Windows.Forms.ListView();
            this.examsTabPage = new System.Windows.Forms.TabPage();
            this.examsListView = new System.Windows.Forms.ListView();
            this.itemsTabControl.SuspendLayout();
            this.materialsTabPage.SuspendLayout();
            this.exercisesTabPage.SuspendLayout();
            this.examsTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(95, 0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(118, 25);
            this.nameLabel.TabIndex = 1;
            this.nameLabel.Text = "nameLabel";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(3, 25);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(84, 13);
            this.descriptionLabel.TabIndex = 2;
            this.descriptionLabel.Text = "descriptionLabel";
            // 
            // addItem
            // 
            this.addItem.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addItem.Location = new System.Drawing.Point(116, 210);
            this.addItem.Name = "addItem";
            this.addItem.Size = new System.Drawing.Size(75, 23);
            this.addItem.TabIndex = 3;
            this.addItem.Text = "Add item";
            this.addItem.UseVisualStyleBackColor = true;
            this.addItem.Click += new System.EventHandler(this.addItem_Click);
            // 
            // itemsTabControl
            // 
            this.itemsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.itemsTabControl.Controls.Add(this.materialsTabPage);
            this.itemsTabControl.Controls.Add(this.exercisesTabPage);
            this.itemsTabControl.Controls.Add(this.examsTabPage);
            this.itemsTabControl.Location = new System.Drawing.Point(6, 51);
            this.itemsTabControl.Name = "itemsTabControl";
            this.itemsTabControl.SelectedIndex = 0;
            this.itemsTabControl.Size = new System.Drawing.Size(297, 153);
            this.itemsTabControl.TabIndex = 4;
            // 
            // materialsTabPage
            // 
            this.materialsTabPage.Controls.Add(this.materialsListView);
            this.materialsTabPage.Location = new System.Drawing.Point(4, 22);
            this.materialsTabPage.Name = "materialsTabPage";
            this.materialsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.materialsTabPage.Size = new System.Drawing.Size(289, 127);
            this.materialsTabPage.TabIndex = 0;
            this.materialsTabPage.Text = "materials";
            this.materialsTabPage.UseVisualStyleBackColor = true;
            // 
            // materialsListView
            // 
            this.materialsListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.materialsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialsListView.Location = new System.Drawing.Point(3, 3);
            this.materialsListView.Name = "materialsListView";
            this.materialsListView.Size = new System.Drawing.Size(283, 121);
            this.materialsListView.TabIndex = 0;
            this.materialsListView.UseCompatibleStateImageBehavior = false;
            this.materialsListView.View = System.Windows.Forms.View.List;
            this.materialsListView.ItemActivate += new System.EventHandler(this.materialsListView_ItemActivate);
            // 
            // exercisesTabPage
            // 
            this.exercisesTabPage.Controls.Add(this.exercisesListView);
            this.exercisesTabPage.Location = new System.Drawing.Point(4, 22);
            this.exercisesTabPage.Name = "exercisesTabPage";
            this.exercisesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.exercisesTabPage.Size = new System.Drawing.Size(289, 127);
            this.exercisesTabPage.TabIndex = 1;
            this.exercisesTabPage.Text = "exercises";
            this.exercisesTabPage.UseVisualStyleBackColor = true;
            // 
            // exercisesListView
            // 
            this.exercisesListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.exercisesListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exercisesListView.Location = new System.Drawing.Point(3, 3);
            this.exercisesListView.Name = "exercisesListView";
            this.exercisesListView.Size = new System.Drawing.Size(283, 121);
            this.exercisesListView.TabIndex = 0;
            this.exercisesListView.UseCompatibleStateImageBehavior = false;
            this.exercisesListView.View = System.Windows.Forms.View.List;
            this.exercisesListView.ItemActivate += new System.EventHandler(this.exercisesListView_ItemActivate);
            // 
            // examsTabPage
            // 
            this.examsTabPage.Controls.Add(this.examsListView);
            this.examsTabPage.Location = new System.Drawing.Point(4, 22);
            this.examsTabPage.Name = "examsTabPage";
            this.examsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.examsTabPage.Size = new System.Drawing.Size(289, 127);
            this.examsTabPage.TabIndex = 2;
            this.examsTabPage.Text = "exams";
            this.examsTabPage.UseVisualStyleBackColor = true;
            // 
            // examsListView
            // 
            this.examsListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.examsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.examsListView.Location = new System.Drawing.Point(3, 3);
            this.examsListView.Name = "examsListView";
            this.examsListView.Size = new System.Drawing.Size(283, 121);
            this.examsListView.TabIndex = 0;
            this.examsListView.UseCompatibleStateImageBehavior = false;
            this.examsListView.View = System.Windows.Forms.View.List;
            this.examsListView.ItemActivate += new System.EventHandler(this.examsListView_ItemActivate);
            // 
            // RepositoryContentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.itemsTabControl);
            this.Controls.Add(this.addItem);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.nameLabel);
            this.Enabled = false;
            this.Name = "RepositoryContentControl";
            this.Size = new System.Drawing.Size(306, 236);
            this.itemsTabControl.ResumeLayout(false);
            this.materialsTabPage.ResumeLayout(false);
            this.exercisesTabPage.ResumeLayout(false);
            this.examsTabPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.Button addItem;
        private System.Windows.Forms.TabControl itemsTabControl;
        private System.Windows.Forms.TabPage materialsTabPage;
        private System.Windows.Forms.TabPage exercisesTabPage;
        private System.Windows.Forms.ListView materialsListView;
        private System.Windows.Forms.ListView exercisesListView;
        private System.Windows.Forms.TabPage examsTabPage;
        private System.Windows.Forms.ListView examsListView;
    }
}
