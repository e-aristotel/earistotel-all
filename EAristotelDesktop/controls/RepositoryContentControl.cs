﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDesktop.forms.add_item;
using EAristotelDesktop.forms.add_item.exam;
using EAristotelDesktop.util;
using EAristotelDomain.model;
using EAristotelDomain.model.repository;

namespace EAristotelDesktop.controls
{
    public partial class RepositoryContentControl : UserControl
    {
        public RepositoryContentControl()
        {
            InitializeComponent();
        }

        private LeRepository _repository;

        public delegate void AddItemHandler(int repositoryId);

        public delegate void MaterialSelectedHandler(int materialId);

        public delegate void ExerciseSelectedHandler(int exerciseId);

        public delegate void ExamSelectedHandler(int examId);

        public event AddItemHandler AddItemRequested;
        public event MaterialSelectedHandler MaterialSelected;
        public event ExerciseSelectedHandler ExerciseSelected;
        public event ExamSelectedHandler ExamSelected;

        public LeRepository Repository
        {
            get { return _repository; }
            set
            {
                if (value == null)
                {
                    Enabled = false;
                    return;
                }

                _repository = value;
                Visible = true;
                Enabled = true;

                nameLabel.Text = _repository.Name;
                descriptionLabel.Visible = false;//todo

                ListViewFiller.RepopulateListView(_repository.Materials, m =>
                {
                    string textString;
                    if (string.IsNullOrWhiteSpace(m.Author))
                    {
                        textString = m.Name;
                    }
                    else
                    {
                        textString = m.Name + " (" + m.Author + ")";
                    }
                    return textString;
                }, materialsListView);
                ListViewFiller.RepopulateListView(_repository.Exercises, e => e.Name, exercisesListView);
                ListViewFiller.RepopulateListView(_repository.Exams, e => e.Name, examsListView);
            }
        }

        

        private void addItem_Click(object sender, EventArgs e)
        {
            if (_repository != null) AddItemRequested?.Invoke(_repository.Id);
        }

        private void materialsListView_ItemActivate(object sender, EventArgs e)
        {
            var tag = materialsListView.SelectedItems[0].Tag;
            if (tag is int)
            {
                var materialId = (int)tag;
                MaterialSelected?.Invoke(materialId);
            }
        }

        private void exercisesListView_ItemActivate(object sender, EventArgs e)
        {
            var tag = exercisesListView.SelectedItems[0].Tag;
            if (tag is int)
            {
                var exerciseId = (int)tag;
                ExerciseSelected?.Invoke(exerciseId);
            }
        }

        private void examsListView_ItemActivate(object sender, EventArgs e)
        {
            var tag = examsListView.SelectedItems[0].Tag;
            if (tag is int)
            {
                var examId = (int)tag;
                ExamSelected?.Invoke(examId);
            }
        }
    }
}