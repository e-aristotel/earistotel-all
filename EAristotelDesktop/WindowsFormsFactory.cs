﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDesktop.forms;
using EAristotelDesktop.forms.add_item.exam;
using EAristotelDesktop.util;
using EAristotelDesktop.util.views;
using EAristotelDesktop.util.wrappers;

namespace EAristotelDesktop
{
    internal class WindowsFormsFactory: IWindowsFormsFactory
    {
        public ICreateRepositoryView CreateRepositoryView(List<RepositoryInfoWrapper> repositoryList)
        {
            return new CreateRepositoryForm(repositoryList);
        }

        public IAddExerciseSolutionView CreateAddExerciseSolutionView()
        {
            return new AddExerciseSolutionForm();
        }

        public ILoginView CreateLoginView()
        {
            return new LoginForm();
        }

        public IAddExamTaskView CreateAddExamTaskView()
        {
            return new AddExamTaskForm();
        }
    }
}
