﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EAristotelDesktop.util;
using EAristotelDesktop.util.views;
using EAristotelDesktop.util.wrappers;

namespace EAristotelDesktop.forms
{
    public partial class CreateRepositoryForm : Form, ICreateRepositoryView
    {
        private readonly IList<RepositoryInfoWrapper> _repositoryList;

        public CreateRepositoryForm(IList<RepositoryInfoWrapper> repositoryList)
        {
            _repositoryList = repositoryList;
            InitializeComponent();
        }

        private void CreateRepositoryForm_Load(object sender, EventArgs e)
        {
            foreach (var repositoryInfo in _repositoryList)
            {
                parentRepositoryComboBox.Items.Add(repositoryInfo);
            }
        }

        public string RepositoryName { get; private set; }
        public int? ParentRepositoryId { get; private set; }

        private void createRepositoryButton_Click(object sender, EventArgs e)
        {
            RepositoryName = repositoryNameTextBox.Text;
            ParentRepositoryId = ((RepositoryInfoWrapper) parentRepositoryComboBox.SelectedItem)?.Id;
            Close();
        }
    }
}