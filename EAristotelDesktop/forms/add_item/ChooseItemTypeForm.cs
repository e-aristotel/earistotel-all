﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EAristotelDesktop.util.wizard;

namespace EAristotelDesktop.forms.add_item
{
    public partial class ChooseItemTypeForm : Form, IWizardForm
    {
        private readonly IWizardForm _materialWizardForm;
        private readonly IWizardForm _exerciseWizardForm;
        private readonly IWizardForm _examWizardForm;

        public ChooseItemTypeForm(IWizardForm materialWizardForm, IWizardForm exerciseWizardForm,
            IWizardForm examWizardForm)
        {
            _materialWizardForm = materialWizardForm;
            _exerciseWizardForm = exerciseWizardForm;
            _examWizardForm = examWizardForm;

            _materialWizardForm.Previous = this;
            _exerciseWizardForm.Previous = this;
            _examWizardForm.Previous = this;

            Next = new List<IWizardForm> {materialWizardForm, exerciseWizardForm, examWizardForm};

            InitializeComponent();
        }

        public IWizardForm Previous { get; set; }
        public IEnumerable<IWizardForm> Next { get; }

        private void materialButton_Click(object sender, EventArgs e)
        {
            OnNext?.Invoke(_materialWizardForm);
        }

        private void exerciseButton_Click(object sender, EventArgs e)
        {
            OnNext?.Invoke(_exerciseWizardForm);
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            OnNext?.Invoke(_examWizardForm);
        }

        public event WizardSimpleDelegate OnFinish;
        public event WizardNextFormDelegate OnNext;
        public event WizardSimpleDelegate OnPrevious;
        public event WizardSimpleDelegate OnCancel;

        private void ChooseItemTypeForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsClosed = true;
            OnCancel?.Invoke();
        }

        public bool IsClosed { get; private set; }
    }
}