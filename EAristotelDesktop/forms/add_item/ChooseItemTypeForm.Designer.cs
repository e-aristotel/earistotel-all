﻿namespace EAristotelDesktop.forms.add_item
{
    partial class ChooseItemTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.examButton = new System.Windows.Forms.Button();
            this.materialButton = new System.Windows.Forms.Button();
            this.exerciseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // examButton
            // 
            this.examButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.examButton.Location = new System.Drawing.Point(89, 70);
            this.examButton.Name = "examButton";
            this.examButton.Size = new System.Drawing.Size(119, 23);
            this.examButton.TabIndex = 0;
            this.examButton.Text = "Exam";
            this.examButton.UseVisualStyleBackColor = true;
            this.examButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // materialButton
            // 
            this.materialButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.materialButton.Location = new System.Drawing.Point(89, 12);
            this.materialButton.Name = "materialButton";
            this.materialButton.Size = new System.Drawing.Size(119, 23);
            this.materialButton.TabIndex = 2;
            this.materialButton.Text = "Material";
            this.materialButton.UseVisualStyleBackColor = true;
            this.materialButton.Click += new System.EventHandler(this.materialButton_Click);
            // 
            // exerciseButton
            // 
            this.exerciseButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.exerciseButton.Location = new System.Drawing.Point(89, 41);
            this.exerciseButton.Name = "exerciseButton";
            this.exerciseButton.Size = new System.Drawing.Size(119, 23);
            this.exerciseButton.TabIndex = 1;
            this.exerciseButton.Text = "Exercise";
            this.exerciseButton.UseVisualStyleBackColor = true;
            this.exerciseButton.Click += new System.EventHandler(this.exerciseButton_Click);
            // 
            // ChooseItemTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 106);
            this.Controls.Add(this.exerciseButton);
            this.Controls.Add(this.materialButton);
            this.Controls.Add(this.examButton);
            this.Name = "ChooseItemTypeForm";
            this.Text = "Choose item";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChooseItemTypeForm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button examButton;
        private System.Windows.Forms.Button materialButton;
        private System.Windows.Forms.Button exerciseButton;
    }
}