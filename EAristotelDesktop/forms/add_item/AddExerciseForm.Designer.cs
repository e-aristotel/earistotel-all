﻿namespace EAristotelDesktop.forms.add_item
{
    partial class AddExerciseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.createExerciseButton = new System.Windows.Forms.Button();
            this.addSolutionCheckBox = new System.Windows.Forms.CheckBox();
            this.exerciseDetailsControl1 = new EAristotelDesktop.controls.ExerciseDetailsControl();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.backButton.Location = new System.Drawing.Point(12, 288);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 0;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // createExerciseButton
            // 
            this.createExerciseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.createExerciseButton.Location = new System.Drawing.Point(312, 288);
            this.createExerciseButton.Name = "createExerciseButton";
            this.createExerciseButton.Size = new System.Drawing.Size(75, 23);
            this.createExerciseButton.TabIndex = 7;
            this.createExerciseButton.Text = "Create";
            this.createExerciseButton.UseVisualStyleBackColor = true;
            this.createExerciseButton.Click += new System.EventHandler(this.createExerciseButton_Click);
            // 
            // addSolutionCheckBox
            // 
            this.addSolutionCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addSolutionCheckBox.AutoSize = true;
            this.addSolutionCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addSolutionCheckBox.Location = new System.Drawing.Point(12, 254);
            this.addSolutionCheckBox.Name = "addSolutionCheckBox";
            this.addSolutionCheckBox.Size = new System.Drawing.Size(83, 17);
            this.addSolutionCheckBox.TabIndex = 8;
            this.addSolutionCheckBox.Text = "add solution";
            this.addSolutionCheckBox.UseVisualStyleBackColor = true;
            // 
            // exerciseDetailsControl1
            // 
            this.exerciseDetailsControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseDetailsControl1.Location = new System.Drawing.Point(12, 12);
            this.exerciseDetailsControl1.Name = "exerciseDetailsControl1";
            this.exerciseDetailsControl1.Size = new System.Drawing.Size(375, 236);
            this.exerciseDetailsControl1.TabIndex = 9;
            // 
            // AddExerciseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 323);
            this.Controls.Add(this.exerciseDetailsControl1);
            this.Controls.Add(this.addSolutionCheckBox);
            this.Controls.Add(this.createExerciseButton);
            this.Controls.Add(this.backButton);
            this.Name = "AddExerciseForm";
            this.Text = "AddExerciseForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddExerciseForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button createExerciseButton;
        private System.Windows.Forms.CheckBox addSolutionCheckBox;
        private controls.ExerciseDetailsControl exerciseDetailsControl1;
    }
}