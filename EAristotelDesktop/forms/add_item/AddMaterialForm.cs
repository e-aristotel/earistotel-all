﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EAristotelBLL.factories;
using EAristotelBLL.services;
using EAristotelDesktop.util.wizard;

namespace EAristotelDesktop.forms.add_item
{
    public partial class AddMaterialForm : Form, IWizardForm
    {
        private readonly IBllServiceProvider _bllServiceProvider;
        private readonly int _repositoryId;
        private readonly string _token;

        public AddMaterialForm(IBllServiceProvider bllServiceProvider, int repositoryId, string token)
        {
            _bllServiceProvider = bllServiceProvider;
            _repositoryId = repositoryId;
            _token = token;
            InitializeComponent();
        }

        public IWizardForm Previous { get; set; }
        public IEnumerable<IWizardForm> Next { get; }
        public event WizardSimpleDelegate OnFinish;
        public event WizardNextFormDelegate OnNext;
        public event WizardSimpleDelegate OnPrevious;
        public event WizardSimpleDelegate OnCancel;

        private void backButton_Click(object sender, EventArgs e)
        {
            OnPrevious?.Invoke();
        }

        public bool IsClosed { get; private set; }

        private void AddMaterialForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsClosed = true;
            OnCancel?.Invoke();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var material = MaterialFactory.CreateMaterial(materialDetailsControl.MaterialName,
                materialDetailsControl.MaterialAuthors,
                materialDetailsControl.MaterialContent);
            _bllServiceProvider.MaterialService.CreateMaterial(_repositoryId, material, _token);
            Close();
        }
    }
}