﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EAristotelBLL.factories;
using EAristotelBLL.services;
using EAristotelDesktop.util;
using EAristotelDesktop.util.wizard;
using EAristotelDomain.model.exam;

namespace EAristotelDesktop.forms.add_item.exam
{
    public partial class AddExamForm : Form, IWizardForm
    {
        private readonly List<ExamTask> _examTasks = new List<ExamTask>();
        private readonly IWindowsFormsFactory _windowsFormsFactory;
        private readonly IBllServiceProvider _bllServiceProvider;
        private readonly int _repositoryId;
        private readonly string _token;

        public AddExamForm(IWindowsFormsFactory windowsFormsFactory, IBllServiceProvider bllServiceProvider, 
            int repositoryId, string token)
        {
            _windowsFormsFactory = windowsFormsFactory;
            _bllServiceProvider = bllServiceProvider;
            _repositoryId = repositoryId;
            _token = token;
            InitializeComponent();
        }

        private void AddExamForm_Load(object sender, EventArgs e)
        {
            UpdateExamTasksListView();
        }

        public IWizardForm Previous { get; set; }
        public IEnumerable<IWizardForm> Next { get; }
        public event WizardSimpleDelegate OnFinish;
        public event WizardNextFormDelegate OnNext;
        public event WizardSimpleDelegate OnPrevious;
        public event WizardSimpleDelegate OnCancel;

        private void backButton_Click(object sender, EventArgs e)
        {
            OnPrevious?.Invoke();
        }

        public bool IsClosed { get; private set; }

        private void AddExamForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsClosed = true;
            OnCancel?.Invoke();
        }

        private void addTaskButton_Click(object sender, EventArgs e)
        {
            var addExamTaskView = _windowsFormsFactory.CreateAddExamTaskView();
            if (DialogResult.OK == addExamTaskView.ShowDialog(this))
            {
                var examTask = ExamFactory.CreateExamTask(addExamTaskView.Content, addExamTaskView.Solutions,
                    addExamTaskView.CorrectSolution);
                _examTasks.Add(examTask);

                UpdateExamTasksListView();
            }
        }

        private void UpdateExamTasksListView()
        {
            ListViewFiller.RepopulateListView(_examTasks, task => task.Content, examTasksListView);
        }

        private void createExamButton_Click(object sender, EventArgs e)
        {
            var exam = ExamFactory.CreateExam(examNameTextBox.Text.Trim(), _examTasks);
            _bllServiceProvider.ExamService.CreateExam(_repositoryId, exam, _token);
            Close();
        }
    }
}
