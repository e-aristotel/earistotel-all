﻿namespace EAristotelDesktop.forms.add_item.exam
{
    partial class AddExamTaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.examTaskContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.examTaskContentWebBrowser = new System.Windows.Forms.WebBrowser();
            this.label2 = new System.Windows.Forms.Label();
            this.examTaskSolutionsListView = new System.Windows.Forms.ListView();
            this.createExamTaskButton = new System.Windows.Forms.Button();
            this.addSolutionButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "content:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(85, 13);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.examTaskContentRichTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.examTaskContentWebBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(353, 215);
            this.splitContainer1.SplitterDistance = 105;
            this.splitContainer1.TabIndex = 1;
            // 
            // examTaskContentRichTextBox
            // 
            this.examTaskContentRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.examTaskContentRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.examTaskContentRichTextBox.Name = "examTaskContentRichTextBox";
            this.examTaskContentRichTextBox.Size = new System.Drawing.Size(353, 105);
            this.examTaskContentRichTextBox.TabIndex = 0;
            this.examTaskContentRichTextBox.Text = "";
            this.examTaskContentRichTextBox.TextChanged += new System.EventHandler(this.examTaskContentRichTextBox_TextChanged);
            // 
            // examTaskContentWebBrowser
            // 
            this.examTaskContentWebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.examTaskContentWebBrowser.Location = new System.Drawing.Point(0, 0);
            this.examTaskContentWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.examTaskContentWebBrowser.Name = "examTaskContentWebBrowser";
            this.examTaskContentWebBrowser.Size = new System.Drawing.Size(353, 106);
            this.examTaskContentWebBrowser.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 239);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "solutions:";
            // 
            // examTaskSolutionsListView
            // 
            this.examTaskSolutionsListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.examTaskSolutionsListView.Location = new System.Drawing.Point(85, 239);
            this.examTaskSolutionsListView.Name = "examTaskSolutionsListView";
            this.examTaskSolutionsListView.Size = new System.Drawing.Size(353, 47);
            this.examTaskSolutionsListView.TabIndex = 3;
            this.examTaskSolutionsListView.UseCompatibleStateImageBehavior = false;
            this.examTaskSolutionsListView.View = System.Windows.Forms.View.List;
            this.examTaskSolutionsListView.SelectedIndexChanged += new System.EventHandler(this.examTaskSolutionsListView_SelectedIndexChanged);
            // 
            // createExamTaskButton
            // 
            this.createExamTaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.createExamTaskButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.createExamTaskButton.Enabled = false;
            this.createExamTaskButton.Location = new System.Drawing.Point(363, 330);
            this.createExamTaskButton.Name = "createExamTaskButton";
            this.createExamTaskButton.Size = new System.Drawing.Size(75, 23);
            this.createExamTaskButton.TabIndex = 4;
            this.createExamTaskButton.Text = "Create";
            this.createExamTaskButton.UseVisualStyleBackColor = true;
            this.createExamTaskButton.Click += new System.EventHandler(this.createExamTaskButton_Click);
            // 
            // addSolutionButton
            // 
            this.addSolutionButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addSolutionButton.Location = new System.Drawing.Point(184, 330);
            this.addSolutionButton.Name = "addSolutionButton";
            this.addSolutionButton.Size = new System.Drawing.Size(75, 23);
            this.addSolutionButton.TabIndex = 5;
            this.addSolutionButton.Text = "Add solution";
            this.addSolutionButton.UseVisualStyleBackColor = true;
            this.addSolutionButton.Click += new System.EventHandler(this.addSolutionButton_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(104, 297);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 300);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "correct solution:";
            // 
            // AddExamTaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 365);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.addSolutionButton);
            this.Controls.Add(this.createExamTaskButton);
            this.Controls.Add(this.examTaskSolutionsListView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label1);
            this.Name = "AddExamTaskForm";
            this.Text = "AddExamTaskForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox examTaskContentRichTextBox;
        private System.Windows.Forms.WebBrowser examTaskContentWebBrowser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView examTaskSolutionsListView;
        private System.Windows.Forms.Button createExamTaskButton;
        private System.Windows.Forms.Button addSolutionButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
    }
}