﻿namespace EAristotelDesktop.forms.add_item.exam
{
    partial class AddExamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.examNameTextBox = new System.Windows.Forms.TextBox();
            this.examTasksListView = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.createExamButton = new System.Windows.Forms.Button();
            this.addTaskButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.backButton.Location = new System.Drawing.Point(12, 228);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 0;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "name:";
            // 
            // examNameTextBox
            // 
            this.examNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.examNameTextBox.Location = new System.Drawing.Point(51, 32);
            this.examNameTextBox.Name = "examNameTextBox";
            this.examNameTextBox.Size = new System.Drawing.Size(288, 20);
            this.examNameTextBox.TabIndex = 2;
            // 
            // examTasksListView
            // 
            this.examTasksListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.examTasksListView.Location = new System.Drawing.Point(50, 84);
            this.examTasksListView.Name = "examTasksListView";
            this.examTasksListView.Size = new System.Drawing.Size(289, 138);
            this.examTasksListView.TabIndex = 3;
            this.examTasksListView.UseCompatibleStateImageBehavior = false;
            this.examTasksListView.View = System.Windows.Forms.View.List;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "tasks:";
            // 
            // createExamButton
            // 
            this.createExamButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.createExamButton.Location = new System.Drawing.Point(264, 228);
            this.createExamButton.Name = "createExamButton";
            this.createExamButton.Size = new System.Drawing.Size(75, 23);
            this.createExamButton.TabIndex = 5;
            this.createExamButton.Text = "Create";
            this.createExamButton.UseVisualStyleBackColor = true;
            this.createExamButton.Click += new System.EventHandler(this.createExamButton_Click);
            // 
            // addTaskButton
            // 
            this.addTaskButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addTaskButton.Location = new System.Drawing.Point(134, 228);
            this.addTaskButton.Name = "addTaskButton";
            this.addTaskButton.Size = new System.Drawing.Size(75, 23);
            this.addTaskButton.TabIndex = 6;
            this.addTaskButton.Text = "Add task";
            this.addTaskButton.UseVisualStyleBackColor = true;
            this.addTaskButton.Click += new System.EventHandler(this.addTaskButton_Click);
            // 
            // AddExamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 263);
            this.Controls.Add(this.addTaskButton);
            this.Controls.Add(this.createExamButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.examTasksListView);
            this.Controls.Add(this.examNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backButton);
            this.Name = "AddExamForm";
            this.Text = "AddExamForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddExamForm_FormClosed);
            this.Load += new System.EventHandler(this.AddExamForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox examNameTextBox;
        private System.Windows.Forms.ListView examTasksListView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button createExamButton;
        private System.Windows.Forms.Button addTaskButton;
    }
}