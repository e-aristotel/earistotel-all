﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDesktop.util;
using EAristotelDesktop.util.views;
using EAristotelDomain.model.exam;

namespace EAristotelDesktop.forms.add_item.exam
{
    public partial class AddExamTaskForm : Form, IAddExamTaskView
    {
        public AddExamTaskForm()
        {
            InitializeComponent();
        }

        private void examTaskContentRichTextBox_TextChanged(object sender, EventArgs e)
        {
            examTaskContentWebBrowser.DocumentText = ScriptUtil.MathJaxHeader + examTaskContentRichTextBox.Text;
        }

        private void createExamTaskButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UpdateSolutionListView()
        {
            ListViewFiller.RepopulateListView(Solutions, solution => solution.Result, examTaskSolutionsListView);
        }

        public string Content => examTaskContentRichTextBox.Text;
        public List<ExamTaskSolution> Solutions { get; } = new List<ExamTaskSolution>();

        private void addSolutionButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Not yet implemented");
            //UpdateSolutionListView();
        }

        private void examTaskSolutionsListView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public ExamTaskSolution CorrectSolution { get; }
    }
}
