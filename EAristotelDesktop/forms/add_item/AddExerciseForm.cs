﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EAristotelBLL.factories;
using EAristotelBLL.services;
using EAristotelDesktop.util;
using EAristotelDesktop.util.wizard;
using EAristotelDomain.model.exercise;

namespace EAristotelDesktop.forms.add_item
{
    public partial class AddExerciseForm : Form, IWizardForm
    {
        private readonly IWindowsFormsFactory _windowsFormsFactory;
        private readonly int _repositoryId;
        private readonly IBllServiceProvider _bllServiceProvider;

        public AddExerciseForm(IWindowsFormsFactory windowsFormsFactory, IBllServiceProvider bllServiceProvider,
            int repositoryId)
        {
            _windowsFormsFactory = windowsFormsFactory;
            _repositoryId = repositoryId;
            _bllServiceProvider = bllServiceProvider;
            InitializeComponent();
        }

        public IWizardForm Previous { get; set; }
        public IEnumerable<IWizardForm> Next { get; }
        public event WizardSimpleDelegate OnFinish;
        public event WizardNextFormDelegate OnNext;
        public event WizardSimpleDelegate OnPrevious;
        public event WizardSimpleDelegate OnCancel;

        private void backButton_Click(object sender, EventArgs e)
        {
            OnPrevious?.Invoke();
        }

        public bool IsClosed { get; private set; }

        private void AddExerciseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            IsClosed = true;
            OnCancel?.Invoke();
        }

        private void createExerciseButton_Click(object sender, EventArgs e)
        {
            var tags = exerciseDetailsControl1.Tags;
            var solutions = new List<ExerciseSolution>();


            if (addSolutionCheckBox.Checked)
            {
                var addExerciseSolutionView = _windowsFormsFactory.CreateAddExerciseSolutionView();
                if (DialogResult.OK == addExerciseSolutionView.ShowDialog(this))
                {
                    var exerciseSolution = ExerciseFactory.CreateExerciseSolution(addExerciseSolutionView.Result,
                        addExerciseSolutionView.StepByStep);
                    solutions.Add(exerciseSolution);
                }
            }

            var exercise = ExerciseFactory.CreateExercise(exerciseDetailsControl1.ExerciseName,
                exerciseDetailsControl1.Content, tags, solutions);

            _bllServiceProvider.ExerciseService.CreateExercise(_repositoryId, exercise);
            Close();
        }
    }
}