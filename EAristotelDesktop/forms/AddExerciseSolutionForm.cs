﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDesktop.util.views;

namespace EAristotelDesktop.forms
{
    public partial class AddExerciseSolutionForm : Form, IAddExerciseSolutionView
    {
        public AddExerciseSolutionForm()
        {
            InitializeComponent();
        }

        public string Result { get; private set; }
        public string StepByStep { get; private set; }

        private void addButton_Click(object sender, EventArgs e)
        {
            Result = exerciseSolutionDetailsControl1.SolutionResult;
            StepByStep = exerciseSolutionDetailsControl1.SolutionStepByStep;
            Close();
        }
    }
}