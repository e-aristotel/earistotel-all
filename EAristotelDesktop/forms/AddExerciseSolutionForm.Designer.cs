﻿namespace EAristotelDesktop.forms
{
    partial class AddExerciseSolutionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addButton = new System.Windows.Forms.Button();
            this.exerciseSolutionDetailsControl1 = new EAristotelDesktop.controls.ExerciseSolutionDetailsControl();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.addButton.Location = new System.Drawing.Point(200, 297);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // exerciseSolutionDetailsControl1
            // 
            this.exerciseSolutionDetailsControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseSolutionDetailsControl1.Location = new System.Drawing.Point(13, 13);
            this.exerciseSolutionDetailsControl1.Name = "exerciseSolutionDetailsControl1";
            this.exerciseSolutionDetailsControl1.Size = new System.Drawing.Size(440, 278);
            this.exerciseSolutionDetailsControl1.TabIndex = 4;
            // 
            // AddExerciseSolutionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 332);
            this.Controls.Add(this.exerciseSolutionDetailsControl1);
            this.Controls.Add(this.addButton);
            this.Name = "AddExerciseSolutionForm";
            this.Text = "AddExerciseSolutionForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private controls.ExerciseSolutionDetailsControl exerciseSolutionDetailsControl1;
    }
}