﻿namespace EAristotelDesktop.forms.details
{
    partial class ExerciseDetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exerciseDetailsControl1 = new EAristotelDesktop.controls.ExerciseDetailsControl();
            this.label1 = new System.Windows.Forms.Label();
            this.solutionsListView = new System.Windows.Forms.ListView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cancelButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.addSolutionButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // exerciseDetailsControl1
            // 
            this.exerciseDetailsControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.exerciseDetailsControl1.Enabled = false;
            this.exerciseDetailsControl1.Location = new System.Drawing.Point(3, 3);
            this.exerciseDetailsControl1.Name = "exerciseDetailsControl1";
            this.exerciseDetailsControl1.Size = new System.Drawing.Size(489, 173);
            this.exerciseDetailsControl1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "solutions:";
            // 
            // solutionsListView
            // 
            this.solutionsListView.Activation = System.Windows.Forms.ItemActivation.TwoClick;
            this.solutionsListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.solutionsListView.Location = new System.Drawing.Point(60, 3);
            this.solutionsListView.Name = "solutionsListView";
            this.solutionsListView.Size = new System.Drawing.Size(432, 77);
            this.solutionsListView.TabIndex = 2;
            this.solutionsListView.UseCompatibleStateImageBehavior = false;
            this.solutionsListView.View = System.Windows.Forms.View.List;
            this.solutionsListView.ItemActivate += new System.EventHandler(this.solutionsListView_ItemActivate);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(15, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cancelButton);
            this.splitContainer1.Panel1.Controls.Add(this.editButton);
            this.splitContainer1.Panel1.Controls.Add(this.exerciseDetailsControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.addSolutionButton);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.solutionsListView);
            this.splitContainer1.Size = new System.Drawing.Size(495, 324);
            this.splitContainer1.SplitterDistance = 208;
            this.splitContainer1.TabIndex = 3;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cancelButton.Enabled = false;
            this.cancelButton.Location = new System.Drawing.Point(6, 182);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Visible = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // editButton
            // 
            this.editButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.editButton.Location = new System.Drawing.Point(210, 182);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 4;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // addSolutionButton
            // 
            this.addSolutionButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addSolutionButton.Location = new System.Drawing.Point(210, 86);
            this.addSolutionButton.Name = "addSolutionButton";
            this.addSolutionButton.Size = new System.Drawing.Size(75, 23);
            this.addSolutionButton.TabIndex = 3;
            this.addSolutionButton.Text = "Add solution";
            this.addSolutionButton.UseVisualStyleBackColor = true;
            this.addSolutionButton.Click += new System.EventHandler(this.addSolutionButton_Click);
            // 
            // ExerciseDetailsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 348);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ExerciseDetailsForm";
            this.Text = "ExerciseDetailsForm";
            this.Load += new System.EventHandler(this.ExerciseDetailsForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private controls.ExerciseDetailsControl exerciseDetailsControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView solutionsListView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button addSolutionButton;
    }
}