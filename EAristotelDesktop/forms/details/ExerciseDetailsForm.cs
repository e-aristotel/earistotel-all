﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelBLL.factories;
using EAristotelDesktop.util;
using EAristotelDomain.model.exercise;

namespace EAristotelDesktop.forms.details
{
    public partial class ExerciseDetailsForm : Form
    {
        private readonly Exercise _exercise;
        private readonly IWindowsFormsFactory _windowsFormsFactory;

        public ExerciseDetailsForm(Exercise exercise, IWindowsFormsFactory windowsFormsFactory)
        {
            _exercise = exercise;
            _windowsFormsFactory = windowsFormsFactory;
            InitializeComponent();
        }

        private void ExerciseDetailsForm_Load(object sender, EventArgs e)
        {
            exerciseDetailsControl1.Exercise = _exercise;
            UpdateSolutionsListView();
        }

        private void UpdateSolutionsListView()
        {
            ListViewFiller.RepopulateListView(_exercise.Solutions, solution => solution.Result, solutionsListView);
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (exerciseDetailsControl1.Enabled)
            {
                exerciseDetailsControl1.Enabled = false;
                cancelButton.Visible = false;
                cancelButton.Enabled = false;
                editButton.Text = @"Edit";
                _exercise.Name = exerciseDetailsControl1.ExerciseName;
                _exercise.Content = exerciseDetailsControl1.Content;
                _exercise.Tags = exerciseDetailsControl1.Tags;
            }
            else
            {
                exerciseDetailsControl1.Enabled = true;
                cancelButton.Visible = true;
                cancelButton.Enabled = true;
                editButton.Text = @"Save";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            exerciseDetailsControl1.Enabled = false;
            cancelButton.Visible = false;
            cancelButton.Enabled = false;
            exerciseDetailsControl1.Exercise = _exercise;
        }

        private void addSolutionButton_Click(object sender, EventArgs e)
        {
            var addExerciseSolutionView = _windowsFormsFactory.CreateAddExerciseSolutionView();
            if (DialogResult.OK == addExerciseSolutionView.ShowDialog(this))
            {
                _exercise.AddSolution(ExerciseFactory.CreateExerciseSolution(addExerciseSolutionView.Result,
                    addExerciseSolutionView.StepByStep));
                UpdateSolutionsListView();
            }
        }

        private void solutionsListView_ItemActivate(object sender, EventArgs e)
        {
            var tag = solutionsListView.SelectedItems[0].Tag;
            if (tag is int)
            {
                var solutionId = (int) tag;
                var exerciseSolution = _exercise.Solutions.First(s => s.Id.Equals(solutionId));
                new ExerciseSolutionDetailsForm(exerciseSolution).ShowDialog(this);
                UpdateSolutionsListView();
            }
        }
    }
}
