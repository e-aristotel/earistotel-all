﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model.exercise;

namespace EAristotelDesktop.forms.details
{
    public partial class ExerciseSolutionDetailsForm : Form
    {
        private readonly ExerciseSolution _exerciseSolution;

        public ExerciseSolutionDetailsForm(ExerciseSolution exerciseSolution)
        {
            _exerciseSolution = exerciseSolution;
            InitializeComponent();
        }

        private void ExerciseSolutionDetailsForm_Load(object sender, EventArgs e)
        {
            exerciseSolutionDetailsControl1.ExerciseSolution = _exerciseSolution;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (exerciseSolutionDetailsControl1.Enabled)
            {
                exerciseSolutionDetailsControl1.Enabled = false;
                cancelButton.Visible = false;
                cancelButton.Enabled = false;
                editButton.Text = @"Edit";
                _exerciseSolution.Result = exerciseSolutionDetailsControl1.SolutionResult;
                _exerciseSolution.StepByStep = exerciseSolutionDetailsControl1.SolutionStepByStep;
            }
            else
            {
                exerciseSolutionDetailsControl1.Enabled = true;
                cancelButton.Visible = true;
                cancelButton.Enabled = true;
                editButton.Text = @"Save";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            exerciseSolutionDetailsControl1.Enabled = false;
            cancelButton.Visible = false;
            cancelButton.Enabled = false;
            exerciseSolutionDetailsControl1.ExerciseSolution = _exerciseSolution;
        }
    }
}
