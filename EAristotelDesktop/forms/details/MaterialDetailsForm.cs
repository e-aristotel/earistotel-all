﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelDomain.model.material;

namespace EAristotelDesktop.forms.details
{
    public partial class MaterialDetailsForm : Form
    {
        private readonly Material _material;

        public MaterialDetailsForm(Material material)
        {
            _material = material;
            InitializeComponent();
        }

        private void MaterialDetailsForm_Load(object sender, EventArgs e)
        {
            materialDetailsControl1.Material = _material;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (materialDetailsControl1.Enabled)
            {
                materialDetailsControl1.Enabled = false;
                cancelButton.Visible = false;
                cancelButton.Enabled = false;
                editButton.Text = @"Edit";
                _material.Name = materialDetailsControl1.MaterialName;
                _material.Author = materialDetailsControl1.MaterialAuthors;
                _material.Content = materialDetailsControl1.MaterialContent;
            }
            else
            {
                materialDetailsControl1.Enabled = true;
                cancelButton.Visible = true;
                cancelButton.Enabled = true;
                editButton.Text = @"Save";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            materialDetailsControl1.Enabled = false;
            cancelButton.Visible = false;
            cancelButton.Enabled = false;
            materialDetailsControl1.Material = _material;
        }
    }
}
