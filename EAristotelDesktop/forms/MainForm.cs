﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EAristotelBLL.factories;
using EAristotelBLL.services;
using EAristotelDesktop.forms.details;
using EAristotelDesktop.util;
using EAristotelDesktop.util.wizard;
using EAristotelDesktop.util.wrappers;
using EAristotelDomain.model.repository;

namespace EAristotelDesktop.forms
{
    public partial class MainForm : Form
    {
        private readonly IWindowsFormsFactory _windowsFormsFactory;
        private readonly IBllServiceProvider _bllServiceProvider;
        private string _adminToken;

        public MainForm(IWindowsFormsFactory windowsFormsFactory, IBllServiceProvider bllServiceProvider)
        {
            _windowsFormsFactory = windowsFormsFactory;
            _bllServiceProvider = bllServiceProvider;

            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var loginView = _windowsFormsFactory.CreateLoginView();
            if (DialogResult.OK == loginView.ShowDialog())
            {
                var token = _bllServiceProvider.AccountService.AdminSignIn(loginView.Username, loginView.Password);
                if (token != null)
                {
                    _adminToken = token;
                    LoadData();
                    return;
                }
                MessageBox.Show(@"Invalid username or password", @"Can not sign in");
            }
            Close();
        }

        private void repositoryContentControl_Load(object sender, EventArgs e)
        {
            repositoryContentControl.AddItemRequested += repositoryId =>
            {
                var wizard = WizardFactory.CreateAddItemToRepositoryWizard(_windowsFormsFactory, _bllServiceProvider, repositoryId,
                    _adminToken);
                wizard.OnClose += () => ShowRepositoryContent(repositoryId);
                wizard.Start();
            };
            repositoryContentControl.MaterialSelected += materialId =>
            {
                using (var context = BllContextFactory.CreateContext())
                {
                    var material = _bllServiceProvider.MaterialService.GetMaterial(materialId, context);
                    new MaterialDetailsForm(material).ShowDialog(this);
                    _bllServiceProvider.MaterialService.UpdateMaterial(material, _adminToken, context);
                }
                ShowRepositoryContent(repositoryContentControl.Repository.Id);
            };
            repositoryContentControl.ExerciseSelected += exerciseId =>
            {
                using (var context = BllContextFactory.CreateContext())
                {
                    var exercise = _bllServiceProvider.ExerciseService.GetExercise(exerciseId, context);
                    new ExerciseDetailsForm(exercise, _windowsFormsFactory).ShowDialog(this);
                    _bllServiceProvider.ExerciseService.UpdateExercise(exercise/*, _adminToken*/, context);
                }
                ShowRepositoryContent(repositoryContentControl.Repository.Id);
            };
            repositoryContentControl.ExamSelected += examId =>
            {
                //todo
            };
        }

        private void LoadData()
        {
            repositoryTreeView.Nodes.Clear();
            using (var context = BllContextFactory.CreateContext())
            {
                var rootRepositories = _bllServiceProvider.LeRepositoryService.GetRepositories(context)
                    .Where(r => r.ParentRepository == null);

                AddRepositoriesToTreeView(rootRepositories);
            }
        }

        private void ShowRepositoryContent(int repositoryId)
        {
            repositoryContentControl.Show();
            using (var context = BllContextFactory.CreateContext())
            {
                repositoryContentControl.Repository =
                _bllServiceProvider.LeRepositoryService.GetRepository(repositoryId, context);
            }
        }

        private void AddRepositoriesToTreeView(IEnumerable<LeRepository> repositoryList, TreeNode parentNode = null)
        {
            foreach (var leRepository in repositoryList)
            {
                var treeNode = new TreeNode {Text = leRepository.Name, Tag = leRepository.Id};
                if (parentNode == null)
                {
                    repositoryTreeView.Nodes.Add(treeNode);
                }
                else
                {
                    parentNode.Nodes.Add(treeNode);
                }
                AddRepositoriesToTreeView(leRepository.ChildrenRepositories, treeNode);
            }
        }

        private void repositoryTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag is int)
            {
                var repositoryId = (int) e.Node.Tag;
                ShowRepositoryContent(repositoryId);
            }
        }

        private void addRepositoryMenuItem_Click(object sender, EventArgs e)
        {
            using (var context = BllContextFactory.CreateContext())
            {
                var leRepositories = _bllServiceProvider.LeRepositoryService.GetRepositories(context);
                var repositoryInfoList =
                    leRepositories.Select(
                            leRepository => new RepositoryInfoWrapper {Id = leRepository.Id, Name = leRepository.Name})
                        .ToList();

                var createRepositoryForm = _windowsFormsFactory.CreateRepositoryView(repositoryInfoList);

                if (DialogResult.OK == createRepositoryForm.ShowDialog(this))
                {
                    var name = createRepositoryForm.RepositoryName;
                    var parentRepositoryId = createRepositoryForm.ParentRepositoryId;
                    var parent = parentRepositoryId.HasValue
                        ? _bllServiceProvider.LeRepositoryService.GetRepository(parentRepositoryId.Value, context)
                        : null;

                    _bllServiceProvider.LeRepositoryService.CreateRepository(
                        LeRepositoryFactory.CreateRepository(name, parent), _adminToken, context);
                    LoadData();
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
    }
}