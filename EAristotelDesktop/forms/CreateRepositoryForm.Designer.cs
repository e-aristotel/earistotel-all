﻿namespace EAristotelDesktop.forms
{
    partial class CreateRepositoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parentRepositoryComboBox = new System.Windows.Forms.ComboBox();
            this.repositoryNameTextBox = new System.Windows.Forms.TextBox();
            this.parentRepositoryLabel = new System.Windows.Forms.Label();
            this.repositoryNameLabel = new System.Windows.Forms.Label();
            this.createRepositoryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // parentRepositoryComboBox
            // 
            this.parentRepositoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.parentRepositoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.parentRepositoryComboBox.FormattingEnabled = true;
            this.parentRepositoryComboBox.Location = new System.Drawing.Point(130, 37);
            this.parentRepositoryComboBox.Name = "parentRepositoryComboBox";
            this.parentRepositoryComboBox.Size = new System.Drawing.Size(200, 21);
            this.parentRepositoryComboBox.TabIndex = 1;
            // 
            // repositoryNameTextBox
            // 
            this.repositoryNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.repositoryNameTextBox.Location = new System.Drawing.Point(129, 9);
            this.repositoryNameTextBox.Name = "repositoryNameTextBox";
            this.repositoryNameTextBox.Size = new System.Drawing.Size(200, 20);
            this.repositoryNameTextBox.TabIndex = 0;
            // 
            // parentRepositoryLabel
            // 
            this.parentRepositoryLabel.AutoSize = true;
            this.parentRepositoryLabel.Location = new System.Drawing.Point(12, 40);
            this.parentRepositoryLabel.Name = "parentRepositoryLabel";
            this.parentRepositoryLabel.Size = new System.Drawing.Size(89, 13);
            this.parentRepositoryLabel.TabIndex = 2;
            this.parentRepositoryLabel.Text = "Parent repository:";
            // 
            // repositoryNameLabel
            // 
            this.repositoryNameLabel.AutoSize = true;
            this.repositoryNameLabel.Location = new System.Drawing.Point(12, 12);
            this.repositoryNameLabel.Name = "repositoryNameLabel";
            this.repositoryNameLabel.Size = new System.Drawing.Size(89, 13);
            this.repositoryNameLabel.TabIndex = 3;
            this.repositoryNameLabel.Text = "Repository name:";
            // 
            // createRepositoryButton
            // 
            this.createRepositoryButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.createRepositoryButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.createRepositoryButton.Location = new System.Drawing.Point(129, 84);
            this.createRepositoryButton.Name = "createRepositoryButton";
            this.createRepositoryButton.Size = new System.Drawing.Size(87, 23);
            this.createRepositoryButton.TabIndex = 4;
            this.createRepositoryButton.Text = "Create";
            this.createRepositoryButton.UseVisualStyleBackColor = true;
            this.createRepositoryButton.Click += new System.EventHandler(this.createRepositoryButton_Click);
            // 
            // CreateRepositoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 119);
            this.Controls.Add(this.createRepositoryButton);
            this.Controls.Add(this.repositoryNameLabel);
            this.Controls.Add(this.parentRepositoryLabel);
            this.Controls.Add(this.repositoryNameTextBox);
            this.Controls.Add(this.parentRepositoryComboBox);
            this.Name = "CreateRepositoryForm";
            this.Text = "CreateRepositoryForm";
            this.Load += new System.EventHandler(this.CreateRepositoryForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox parentRepositoryComboBox;
        private System.Windows.Forms.TextBox repositoryNameTextBox;
        private System.Windows.Forms.Label parentRepositoryLabel;
        private System.Windows.Forms.Label repositoryNameLabel;
        private System.Windows.Forms.Button createRepositoryButton;
    }
}