﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDesktop
{
    internal static class ScriptUtil
    {
        public static string MathJaxHeader => @"<script type=""text/javascript"" src=""https://cdn.mathjax.org/mathjax/latest/MathJax.js""></script>";
    }
}
