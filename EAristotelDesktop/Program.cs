﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDesktop.forms;
using EAristotelDesktop.util;

namespace EAristotelDesktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IWindowsFormsFactory windowsFormsFactory = new WindowsFormsFactory();
            IBllServiceProvider bllService = new BllServiceProvider();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(windowsFormsFactory, bllService));
        }
    }
}
