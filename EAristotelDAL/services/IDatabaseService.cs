﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;

namespace EAristotelDAL.services
{
    public interface IDatabaseService
    {
        ISession OpenSession();
        void RecreateDatabase();
        void CreateDatabaseIfMissing();
    }
}
