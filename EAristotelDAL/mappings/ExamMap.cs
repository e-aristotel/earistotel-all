﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exam;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class ExamMap: ClassMap<Exam>
    {
        public ExamMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Name).Not.Nullable();
            HasMany(x => x.ExamTasks).Cascade.All();
            References(x => x.Repository).Not.Nullable();
        }
    }
}
