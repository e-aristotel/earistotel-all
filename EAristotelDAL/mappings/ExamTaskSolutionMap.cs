﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exam;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class ExamTaskSolutionMap : SubclassMap<ExamTaskSolution>
    {
        public ExamTaskSolutionMap()
        {
            References(x => x.ExamTask).Not.Nullable();
        }
    }
}