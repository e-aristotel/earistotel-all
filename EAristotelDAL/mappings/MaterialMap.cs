﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using EAristotelDomain.model.material;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class MaterialMap: ClassMap<Material>
    {
        public MaterialMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Author).Not.Nullable();
            Map(x => x.Content).Not.Nullable();
            References(x => x.Repository).Not.Nullable();
        }
    }
}
