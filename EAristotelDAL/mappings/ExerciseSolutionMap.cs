﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exercise;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class ExerciseSolutionMap: SubclassMap<ExerciseSolution>
    {
        public ExerciseSolutionMap()
        {
            Map(x => x.StepByStep).Not.Nullable();
            References(x => x.Exercise).Column("ExerciseId").Not.Nullable();
        }
    }
}
