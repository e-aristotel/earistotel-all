﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.accounts;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class AdminMap: ClassMap<Admin>
    {
        public AdminMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Username).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
        }
    }
}
