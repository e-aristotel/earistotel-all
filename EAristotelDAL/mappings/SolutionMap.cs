﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class SolutionMap: ClassMap<Solution>
    {
        public SolutionMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Result).Not.Nullable();
        }
    }
}
