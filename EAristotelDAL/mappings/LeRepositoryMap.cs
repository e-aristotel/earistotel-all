﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.repository;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class LeRepositoryMap: ClassMap<LeRepository>
    {
        public LeRepositoryMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Name).Not.Nullable();
            References(x => x.ParentRepository).Nullable();
            HasMany(x => x.ChildrenRepositories).Inverse().Cascade.All();
            HasMany(x => x.Exams).Inverse().Cascade.All();
            HasMany(x => x.Exercises).Inverse().Cascade.All();
            HasMany(x => x.Materials).Inverse().Cascade.All();
        }
    }
}
