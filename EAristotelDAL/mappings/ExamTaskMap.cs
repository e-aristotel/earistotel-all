﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exam;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class ExamTaskMap: ClassMap<ExamTask>
    {
        public ExamTaskMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Content).Not.Nullable();
            HasMany(x => x.OfferedSolutions).Cascade.All();
            References(x => x.CorrectSolution).Nullable();
            References(x => x.Exam).Not.Nullable();
        }
    }
}
