﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model.exercise;
using FluentNHibernate.Mapping;

namespace EAristotelDAL.mappings
{
    internal class ExerciseMap: ClassMap<Exercise>
    {
        public ExerciseMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Name).Not.Nullable();
            Map(x => x.Content).Not.Nullable();
            HasMany(x => x.Solutions).KeyColumn("ExerciseId").Cascade.All();
            References(x => x.BestSolution).Nullable();
            HasMany(x => x.Tags).Element("Tag").Cascade.All();
            References(x => x.Repository).Not.Nullable();
        }
    }
}
