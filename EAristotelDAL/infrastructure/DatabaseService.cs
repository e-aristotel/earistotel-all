﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using EAristotelDAL.mappings;
using EAristotelDAL.services;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace EAristotelDAL.infrastructure
{
    public class DatabaseService : IDatabaseService
    {
        private static readonly ISessionFactory SessionFactory;

        private static readonly string DbPath =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "EAristotelDB.db");

        static DatabaseService()
        {
            SessionFactory = GetConfig().BuildSessionFactory();
        }

        public DatabaseService()
        {
            //RecreateDatabase();
            CreateDatabaseIfMissing();
        }

        public ISession OpenSession()
        {
            try
            {
                return SessionFactory.OpenSession();
            }
            catch (Exception e)
            {
                throw e.InnerException ?? e;
            }
        }

        private static FluentConfiguration GetConfig()
        {
            IPersistenceConfigurer configurator = SQLiteConfiguration.Standard
                .ConnectionString("Data Source=" + DbPath + ";Version=3")
                .AdoNetBatchSize(100)
                .ShowSql().FormatSql();

            return Fluently.Configure()
                .Database(configurator)
                .Mappings(mappings => mappings.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()));
        }

        public void RecreateDatabase()
        {
            GetConfig().ExposeConfiguration(c => new SchemaExport(c).Create(true, true)).BuildConfiguration();
        }

        public void CreateDatabaseIfMissing()
        {
            GetConfig().ExposeConfiguration(c => new SchemaUpdate(c).Execute(true, true)).BuildConfiguration();
        }
    }
}