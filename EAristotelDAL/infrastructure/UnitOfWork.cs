﻿using EAristotelDAL.services;
using EAristotelDomain.util;
using NHibernate;

namespace EAristotelDAL.infrastructure
{
    internal class UnitOfWork : IUnitOfWork
    {
        private static readonly IDatabaseService DatabaseService;

        static UnitOfWork()
        {
            DatabaseService = new DatabaseService();
        }

        internal ISession Session { get; }

        private ITransaction _transaction;

        internal UnitOfWork()
        {
            Session = DatabaseService.OpenSession();
        }

        public void BeginTransaction()
        {
            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                if (_transaction != null && _transaction.IsActive)
                {
                    _transaction.Commit();
                }
            }
            catch
            {
                Rollback();
                throw;
            }
            finally
            {
                _transaction?.Dispose();
            }
        }

        public void Rollback()
        {
            try
            {
                if (_transaction != null && _transaction.IsActive)
                {
                    _transaction.Rollback();
                }
            }
            finally 
            {
                _transaction?.Dispose();
            }
        }

        public void Dispose()
        {
            _transaction?.Dispose();
            Session.Dispose();
        }
    }
}