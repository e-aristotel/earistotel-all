﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.repositories;
using NHibernate;
using NHibernate.Linq;

namespace EAristotelDAL.repositories
{
    internal class StandardNHRepo<T> : IRepository<T>
    {
        private readonly ISession _session;

        internal StandardNHRepo(ISession session)
        {
            _session = session;
        }

        public bool Add(T t)
        {
            object id = _session.Save(t);
            return id != null;
        }

        public bool Update(T t)
        {
            _session.Update(t);

            //test - should be validated
            return true;
        }

        public bool Remove(T t)
        {
            _session.Delete(t);

            //test - should be validated
            return true;
        }

        public T Fetch<TPk>(TPk primaryKey)
        {
            return _session.Get<T>(primaryKey);
        }

        public T Fetch(Expression<Func<T, bool>> predicate)
        {
            //default==null for instance types?
            return _session.Query<T>().Where(predicate).FirstOrDefault();
        }

        public IQueryable<T> FetchMany(Expression<Func<T, bool>> predicate)
        {
            return _session.Query<T>().Where(predicate);
        }

        public IQueryable<T> FetchAll()
        {
            return _session.Query<T>();
        }
    }
}