﻿using System;
using EAristotelDomain.repositories;
using EAristotelDomain.util;

namespace EAristotelDAL.repositories
{
    public interface IDalContext: IDisposable
    {
        IUnitOfWork UnitOfWork { get; }
        IAdminRepo AdminRepo { get; }
        IExamRepo ExamRepo { get; }
        IExerciseRepo ExerciseRepo { get; }
        IExerciseSolutionRepo ExerciseSolutionRepo { get; }
        ILeRepositoryRepo LeRepositoryRepo { get; }
        IMaterialRepo MaterialRepo { get; }
    }
}
