﻿using EAristotelDomain.model.repository;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    internal class LeRepositoryRepo : NamedRepositoryNH<LeRepository>, ILeRepositoryRepo
    {
        internal LeRepositoryRepo(ISession session) : base(session)
        {
        }
    }
}
