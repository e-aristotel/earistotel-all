﻿using EAristotelDomain.model.exercise;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    internal class ExerciseRepo : NamedRepositoryNH<Exercise>, IExerciseRepo
    {
        internal ExerciseRepo(ISession session) : base(session)
        {
        }
    }
}