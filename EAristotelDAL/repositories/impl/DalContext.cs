﻿using EAristotelDAL.infrastructure;
using EAristotelDomain.model.accounts;
using EAristotelDomain.repositories;
using EAristotelDomain.util;

namespace EAristotelDAL.repositories.impl
{
    /// <summary>
    /// Class that contains unit of work and lazy-constructed repositories
    /// </summary>
    public sealed class DalContext : IDalContext
    {
        /// <summary>
        /// temp
        /// </summary>
        static DalContext()
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var tempAdminRepo = new AdminRepo(unitOfWork.Session);
                var profesor = tempAdminRepo.Fetch(a => a.Username.Equals("admin"));
                if (profesor == null)
                {
                    tempAdminRepo.Add(new Admin {Username = "admin", Password = "password"});
                }
            }
        }

        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        public IUnitOfWork UnitOfWork => _unitOfWork;

        private AdminRepo _adminRepo;
        public IAdminRepo AdminRepo => _adminRepo ?? (_adminRepo = new AdminRepo(_unitOfWork.Session));

        private ExamRepo _examRepo;
        public IExamRepo ExamRepo => _examRepo ?? (_examRepo = new ExamRepo(_unitOfWork.Session));

        private ExerciseRepo _exerciseRepo;
        public IExerciseRepo ExerciseRepo => _exerciseRepo ?? (_exerciseRepo = new ExerciseRepo(_unitOfWork.Session));

        private ExerciseSolutionRepo _exerciseSolutionRepo;

        public IExerciseSolutionRepo ExerciseSolutionRepo
            => _exerciseSolutionRepo ?? (_exerciseSolutionRepo = new ExerciseSolutionRepo(_unitOfWork.Session));

        private LeRepositoryRepo _leRepositoryRepo;

        public ILeRepositoryRepo LeRepositoryRepo
            => _leRepositoryRepo ?? (_leRepositoryRepo = new LeRepositoryRepo(_unitOfWork.Session));

        private MaterialRepo _materialRepo;
        public IMaterialRepo MaterialRepo => _materialRepo ?? (_materialRepo = new MaterialRepo(_unitOfWork.Session));

        public void Dispose()
        {
            _unitOfWork?.Dispose();
        }
    }
}