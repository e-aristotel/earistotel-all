﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    class NamedRepositoryNH<T> : StandardNHRepo<T>, INamedRepository<T> where T:IHasName
    {
        public NamedRepositoryNH(ISession session) : base(session)
        {
        }

        public IList<T> SearchByName(string query)
        {
            return FetchMany(entity => entity.Name.ToLower().Contains(query.Trim().ToLower())).ToList();
        }
    }
}
