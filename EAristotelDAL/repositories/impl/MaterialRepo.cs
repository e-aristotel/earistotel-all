﻿using EAristotelDomain.model.material;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    internal class MaterialRepo: NamedRepositoryNH<Material>, IMaterialRepo
    {
        internal MaterialRepo(ISession session) : base(session)
        {
        }
    }
}
