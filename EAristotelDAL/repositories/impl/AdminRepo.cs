﻿using EAristotelDomain.model.accounts;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    internal class AdminRepo : StandardNHRepo<Admin>, IAdminRepo
    {
        internal AdminRepo(ISession session) : base(session)
        {
        }
    }
}