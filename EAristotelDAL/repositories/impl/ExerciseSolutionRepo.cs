﻿using EAristotelDomain.model.exercise;
using EAristotelDomain.repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelDAL.repositories.impl
{
    class ExerciseSolutionRepo : StandardNHRepo<ExerciseSolution>, IExerciseSolutionRepo
    {
        internal ExerciseSolutionRepo(ISession session) : base(session)
        {
        }
    }
}
