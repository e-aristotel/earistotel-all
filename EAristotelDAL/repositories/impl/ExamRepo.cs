﻿using EAristotelDomain.model.exam;
using EAristotelDomain.repositories;
using NHibernate;

namespace EAristotelDAL.repositories.impl
{
    internal class ExamRepo : NamedRepositoryNH<Exam>, IExamRepo
    {
        internal ExamRepo(ISession session) : base(session)
        {
        }
    }
}
