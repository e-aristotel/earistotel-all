﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EAristotelBLL;
using EAristotelBLL.services;
using EAristotelDAL.repositories;
using EAristotelDomain.model.exam;
using EAristotelDomain.model.exercise;
using EAristotelDomain.model.material;
using EAristotelDomain.model.repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace BLLTestProject
{
    [TestClass]
    public class SearchTests
    {
        [TestMethod]
        public void Search_With_Query_CallsAll_DALRepositories_With_Query_Words()
        {
            string query = "Mali Mate hoda ulicom";

            LeRepository repo=new LeRepository();
            repo.Name = "bla bla mali bla bla";
            Exam exam=new Exam();
            exam.Name = "mwa hodamwa mwa";
            Material material=new Material();
            material.Name = "vau vau mate";
            Exercise exercise=new Exercise();
            exercise.Name = "ulicom fau fau";
          
            List<LeRepository> repoList=new List<LeRepository>();
            repoList.Add(repo);
            List<Exam> examList = new List<Exam>();
            examList.Add(exam);
            List<Material> materialList = new List<Material>();
            materialList.Add(material);
            List<Exercise> exerciseList = new List<Exercise>();
            exerciseList.Add(exercise);

            //Arrange
            //this is a heavy component(db access, repos,...) so we mock it
            IDalContext context = Substitute.For<IDalContext>();           
            BllContext contextBll = new BllContext(context);

            context.ExamRepo.SearchByName("hoda").Returns(examList);
            context.LeRepositoryRepo.SearchByName("mali").Returns(repoList);
            context.ExerciseRepo.SearchByName("ulicom").Returns(exerciseList);
            context.MaterialRepo.SearchByName("mate").Returns(materialList);

            //SUT
            ILeRepositoryService leRepositoryService = new BllServiceProvider().LeRepositoryService;

            //act
            LeRepository searchResult = leRepositoryService.SearchEverythingForRepo(query, contextBll);

            //Assert
            context.ExamRepo.Received(4);
            context.LeRepositoryRepo.Received(4);
            context.ExerciseRepo.Received(4);
            context.MaterialRepo.Received(4);

            Assert.IsTrue(searchResult.ChildrenRepositories.Contains(repo));
            Assert.IsTrue(searchResult.Exams.Contains(exam));
            Assert.IsTrue(searchResult.Exercises.Contains(exercise));
            Assert.IsTrue(searchResult.Materials.Contains(material));
        }
    }
}
