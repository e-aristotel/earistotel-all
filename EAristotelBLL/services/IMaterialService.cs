﻿using EAristotelDomain.model.material;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface IMaterialService
    {
        void CreateMaterial(int repositoryId, Material material, string token, BllContext context = null);

        void UpdateMaterial(Material material, string token, BllContext context = null);

        void RemoveMaterial(int materialId, string token, BllContext context = null);

        Material GetMaterial(int materialId, BllContext context = null);

        IList<Material> GetMaterials(int repositoryId, BllContext context = null);
        IList<Material> Search(string query, BllContext context=null);
        IList<Material> GetAllMaterials();
    }
}
