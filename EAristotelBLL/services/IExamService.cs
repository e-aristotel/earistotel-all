﻿using EAristotelDomain.model.exam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface IExamService
    {
        void CreateExam(int repoId, Exam exam, string token, BllContext context = null);

        void UpdateExam(Exam exam, int repoId, string token, BllContext context = null);

        void DeleteExam(int examId, string token, BllContext context = null);

        Exam GetExam(int examId, BllContext context = null);

        IList<Exam> GetExams(BllContext context = null);

        void AddExamTask(ExamTask examTask,  int examId, string token, BllContext context = null);

        void RemoveExamTask(int examTaskId, int examId, string token, BllContext context = null);
        IList<Exam> Search(string query, BllContext context=null);
        IList<Exam> GetAllExams();
        ExamEvaluationModel EvaluateExam(ExamEvaluationModel submittedExam, BllContext context=null);
    }
}