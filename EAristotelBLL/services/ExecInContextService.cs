﻿using System;
using System.Collections.Generic;
using EAristotelDomain.model.repository;

namespace EAristotelBLL.services
{
    /// <summary>
    /// Util service to reduce boilerplate when executing code inside Context
    /// </summary>
    public static class ExecInContextService
    {
        private static T ExecInNonNullContext<T>(BllContext context, Func<BllContext, T> func)
        {
            context.UnitOfWork.BeginTransaction();
            try
            {
                var result = func(context);
                context.UnitOfWork.Commit();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                context.UnitOfWork.Rollback();
                throw;
            }
        }

        /// <summary>
        /// Executes given function inside transaction using given Context. 
        /// Does NOT dispose given Context and underlying UnitOfWork and Session,
        /// because the caller is responsible for Context lifetime.
        /// If given Context is null, executes given function using new Context, which will be safely disposed upon finish.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="context">Nullable context</param>
        /// <param name="func">Function to execute</param>
        /// <returns></returns>
        public static T ExecInContext<T>(BllContext context, Func<BllContext, T> func)
        {
            return context != null
                ? ExecInNonNullContext(context, func)
                : ExecUsingNewContext(func);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">Nullable context</param>
        /// <param name="action">Action to perform</param>
        public static void ExecInContext(BllContext context, Action<BllContext> action)
        {
            ExecInContext(context, ActionToFunc(action));
        }

        /// <summary>
        /// Performs the given function inside transaction using new Context.
        /// Safely disposes Context and underlying UnitOfWork, Session and Transaction.
        /// Begins transaction and commits it if given func didn't throw any exception.
        /// Rolls back active transaction if exception occured anywhere in given func.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func">Function to execute</param>
        /// <returns></returns>
        public static T ExecUsingNewContext<T>(Func<BllContext, T> func)
        {
            using (var context = BllContextFactory.CreateContext())
            {
                return ExecInNonNullContext(context, func);
            }
        }

        /// <summary>
        /// Performs the given action inside transaction using new Context.
        /// Safely disposes Context and underlying UnitOfWork, Session and Transaction.
        /// Begins transaction and commits it if given action didn't throw any exception.
        /// Rolls back active transaction if exception occured anywhere in given action.
        /// </summary>
        /// <param name="action">Action to perform</param>
        public static void ExecUsingNewContext(Action<BllContext> action)
        {
            ExecUsingNewContext(ActionToFunc(action));
        }

        private static Func<BllContext, int> ActionToFunc(Action<BllContext> action) => c =>
        {
            action(c);
            return -7;
        };

        public static void LoadSubtreeItem(object subtreeItem)
        {
            //should be loaded on access
        }

        public static void LoadSubtreeList<O>(IList<O> listOfItems)
        {
            if (listOfItems.Count != 0)
            {
                //should load all items through lazy load
                LoadSubtreeItem(listOfItems[0]);
            }
        }
    }
}