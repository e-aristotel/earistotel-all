﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.model;
using EAristotelDomain.repositories;
using WebGrease.Css.Extensions;

namespace EAristotelBLL.services
{
    public class SearchUtilities
    {
        public static List<T> SimpleSearch<T>(string query, INamedRepository<T> repository) where T:IHasName
        {
            List<T> searchResults=new List<T>();

            query.ToLower().Split(' ').ForEach(qw => searchResults.AddRange(
                repository.SearchByName(qw)));

            return searchResults;
        }
    }
}
