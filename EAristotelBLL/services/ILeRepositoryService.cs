﻿using EAristotelDomain.model.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface ILeRepositoryService
    {
        void CreateRepository(LeRepository repository, string token, BllContext context = null);

        void RemoveRepository(int repositoryId, string token, BllContext context = null);

        LeRepository GetRepository(int repositoryId, BllContext context = null);

        IList<LeRepository> GetRepositories(BllContext context = null);
        IList<LeRepository> GetTopLevelRepositories(BllContext context = null);

        /// <summary>
        /// Searches all the repositories. Doesn't search repository content, 
        /// only repository descriptors.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        List<LeRepository> Search(string query, BllContext context = null);

        /// <summary>
        /// Searches everything(repos, materials, exams, excercises), 
        /// and pack it all up in a parent repo for convenience.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        LeRepository SearchEverythingForRepo(string query, BllContext context = null);
    }
}