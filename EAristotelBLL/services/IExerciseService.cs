﻿using EAristotelDomain.model.exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface IExerciseService
    {
        void CreateExercise(int repositoryId, Exercise exercise, BllContext context = null);

        void UpdateExercise(Exercise exercise, BllContext context = null);

        void RemoveExercise(int exerciseId, string token, BllContext context = null);

        Exercise GetExercise(int exerciseId, BllContext context = null);

        IList<Exercise> GetExercises(int repositoryId, BllContext context = null);

        IList<Exercise> GetAllExercises(BllContext context = null);

        void AddSolution(int exerciseId, ExerciseSolution solution, BllContext context = null);

        void RemoveSolution(int exerciseSolutionId, string token, BllContext context = null);

        void MarkSolutionAsPreffered(int exerciseSolutionId, string token, BllContext context = null);
        IList<Exercise> Search(string query, BllContext context=null);
    }
}