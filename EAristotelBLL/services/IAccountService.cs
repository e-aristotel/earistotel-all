﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface IAccountService
    {
        /// <summary>
        /// Checks if username and password are OK and returns generated token for this login session.
        /// If username or password is invalid, returns null token.
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="password">user password</param>
        /// <returns>string token, null if username or password is invalid</returns>
        string AdminSignIn(string username, string password);
    }
}
