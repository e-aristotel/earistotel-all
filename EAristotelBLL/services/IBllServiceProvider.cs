﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.services
{
    public interface IBllServiceProvider
    {
        IAccountService AccountService { get; }
        IExamService ExamService { get; }
        IExerciseService ExerciseService { get; }
        ILeRepositoryService LeRepositoryService { get; }
        IMaterialService MaterialService { get; }
    }
}
