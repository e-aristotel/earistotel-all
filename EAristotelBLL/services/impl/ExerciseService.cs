﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelBLL.authorization;
using EAristotelDomain.model.exercise;

namespace EAristotelBLL.services.impl
{
    internal class ExerciseService: IExerciseService
    {
        private readonly PermissionManager _permissionManager;

        public ExerciseService(PermissionManager permissionManager)
        {
            _permissionManager = permissionManager;
        }

        public void CreateExercise(int repositoryId, Exercise exercise, BllContext context = null)
        {
            ExecInContextService.ExecInContext(context, c =>
            {
                var leRepo = c.LeRepositoryRepo.Fetch(repositoryId);
                exercise.Repository = leRepo;
                c.ExerciseRepo.Add(exercise);
            });
        }

        public void UpdateExercise(Exercise exercise, BllContext context = null)
        {
            ExecInContextService.ExecInContext(context, c => c.ExerciseRepo.Update(exercise));
        }

        public void RemoveExercise(int exerciseId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXERCISE_DELETE);
            ExecInContextService.ExecInContext(context, c =>
            {
                var exercise = c.ExerciseRepo.Fetch(exerciseId);
                if (exercise != null)
                {
                    c.ExerciseRepo.Remove(exercise);
                }
            });
        }

        public Exercise GetExercise(int exerciseId, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.ExerciseRepo.Fetch(exerciseId));
        }

        public IList<Exercise> GetExercises(int repositoryId, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.ExerciseRepo.FetchMany(e => e.Repository.Id.Equals(repositoryId))).ToList();
        }

        public IList<Exercise> GetAllExercises(BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.ExerciseRepo.FetchAll().ToList());
        }

        public void AddSolution(int exerciseId, ExerciseSolution solution, BllContext context = null)
        {
            ExecInContextService.ExecInContext(context, c =>
            {
                var exercise = c.ExerciseRepo.Fetch(exerciseId);
                if (exercise != null)
                {
                    solution.Exercise = exercise;
                    c.ExerciseSolutionRepo.Add(solution);
                }
            });
        }

        public void RemoveSolution(int exerciseSolutionId, string token,BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.SOLUTION_DELETE);
            ExecInContextService.ExecInContext(context, c =>
            {
                var exercise = c.ExerciseRepo.Fetch(e => e.Solutions.Any(s => s.Id.Equals(exerciseSolutionId)));
                var exerciseSolution = exercise.Solutions.First(s => s.Id.Equals(exerciseSolutionId));
                exercise.Solutions.Remove(exerciseSolution);
                if (Equals(exercise.BestSolution, exerciseSolution))
                {
                    exercise.BestSolution = null;
                }
                c.ExerciseRepo.Update(exercise);
            });
        }

        public void MarkSolutionAsPreffered(int exerciseSolutionId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.CHOOSE_SOLUTION);
            ExecInContextService.ExecInContext(context, c =>
            {
                var exercise = c.ExerciseRepo.Fetch(e => e.Solutions.Any(s => s.Id.Equals(exerciseSolutionId)));
                var exerciseSolution = exercise.Solutions.First(s => s.Id.Equals(exerciseSolutionId));
                exercise.BestSolution = exerciseSolution;
                c.ExerciseRepo.Update(exercise);
            });
        }

        public IList<Exercise> Search(string query, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => SearchUtilities.SimpleSearch(query, c.ExerciseRepo));
        }
    }
}
