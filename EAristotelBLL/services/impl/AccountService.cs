﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelBLL.authorization;

namespace EAristotelBLL.services.impl
{
    internal class AccountService : IAccountService
    {
        private readonly PermissionManager _permissionManager;

        public AccountService(PermissionManager permissionManager)
        {
            _permissionManager = permissionManager;
        }

        public string AdminSignIn(string username, string password)
        {
            return ExecInContextService.ExecUsingNewContext(c =>
            {
                string token = null;
                var admin = c.AdminRepo.Fetch(a => a.Username.Equals(username));
                if (admin != null && admin.Password.Equals(password))
                {
                    token =
                        _permissionManager.SavePermissionBundle(PermissionBundleFactory.CreateAdminPermissionBundle());
                }
                return token;
            });
        }
    }
}