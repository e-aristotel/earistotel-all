﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelBLL.authorization;
using EAristotelDomain.model.exam;

namespace EAristotelBLL.services.impl
{
    internal class ExamService : IExamService
    {
        private readonly PermissionManager _permissionManager;

        public ExamService(PermissionManager permissionManager)
        {
            _permissionManager = permissionManager;
        }

        public void CreateExam(int repoId, Exam exam, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXAM_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                exam.Repository = c.LeRepositoryRepo.Fetch(repoId);
                c.ExamRepo.Add(exam);
            });
            //test-Cascade should save all the exam tasks and their solutions
        }

        public void UpdateExam(Exam exam, int repoId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXAM_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                exam.Repository = c.LeRepositoryRepo.Fetch(repoId);
                //test-Cascade should update all the exam tasks and their solutions
                c.ExamRepo.Update(exam);
            });
        }

        public void DeleteExam(int examId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXAM_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                //test-Cascade should delete all the exam tasks and their solutions
                var exam = c.ExamRepo.Fetch(examId);
                c.ExamRepo.Remove(exam);
            });
        }

        public Exam GetExam(int examId, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.ExamRepo.Fetch(examId));
        }

        public IList<Exam> GetExams(BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.ExamRepo.FetchAll().ToList());
        }

        public void AddExamTask(ExamTask examTask,  int examId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXAM_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                var exam = c.ExamRepo.Fetch(examId);
                exam.AddTask(examTask);
                c.ExamRepo.Update(exam);
            });
        }

        public void RemoveExamTask(int examTaskId,  int examId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.EXAM_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                var exam = c.ExamRepo.Fetch(examId);
                var task = exam.ExamTasks.First(examTask => examTask.Id.Equals(examTaskId));
                exam.RemoveTask(task);
                c.ExamRepo.Update(exam);
            });
        }

        public IList<Exam> Search(string query, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => SearchUtilities.SimpleSearch(query, c.ExamRepo));
        }

        public IList<Exam> GetAllExams()
        {
            return ExecInContextService.ExecInContext(null, c=> c.ExamRepo.FetchAll().ToList());
        }

        public ExamEvaluationModel EvaluateExam(ExamEvaluationModel submittedExam, BllContext context=null)
        {
            return ExecInContextService.ExecInContext(context,
                c => c.ExamRepo.Fetch(submittedExam.Id).Evaluate(submittedExam));
        }
    }
}