﻿using System.Collections.Generic;
using System.Linq;
using EAristotelBLL.authorization;
using EAristotelDomain.model.repository;
using EAristotelDomain.util.exceptions;

namespace EAristotelBLL.services.impl
{
    internal class LeRepositoryService : ILeRepositoryService
    {
        private readonly PermissionManager _permissionManager;
        private readonly BllServiceProvider _bllServiceProvider;

        public LeRepositoryService(PermissionManager permissionManager, BllServiceProvider bllServiceProvider)
        {
            _permissionManager = permissionManager;
            _bllServiceProvider = bllServiceProvider;
        }

        public void CreateRepository(LeRepository repository, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.REPOSITORY_CRUD);
            ExecInContextService.ExecInContext(context, c => c.LeRepositoryRepo.Add(repository));
        }

        public IList<LeRepository> GetRepositories(BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.LeRepositoryRepo.FetchAll().ToList());
        }

        public IList<LeRepository> GetTopLevelRepositories(BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context,
                c => c.LeRepositoryRepo.FetchMany(repo => repo.ParentRepository == null).ToList());
        }

        public LeRepository SearchEverythingForRepo(string query, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c =>
            {
                var searchResult = new LeRepository
                {
                    ChildrenRepositories = Search(query, context),
                    Exams = _bllServiceProvider.ExamService.Search(query, context),
                    Exercises = _bllServiceProvider.ExerciseService.Search(query, context),
                    Materials = _bllServiceProvider.MaterialService.Search(query, context)
                };

                return searchResult;
            });
        }

        public List<LeRepository> Search(string query, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context,
                c => SearchUtilities.SimpleSearch(query, c.LeRepositoryRepo));
        }

        public LeRepository GetRepository(int repositoryId, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c =>
            {
                LeRepository repository = c.LeRepositoryRepo.Fetch(repositoryId);

                return repository;
            });
        }

        public void RemoveRepository(int repositoryId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.REPOSITORY_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                var repository = c.LeRepositoryRepo.Fetch(repositoryId);
                if (repository != null)
                {
                    c.LeRepositoryRepo.Remove(repository);
                }
            });
        }
    }
}