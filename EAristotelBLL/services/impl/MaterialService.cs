﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelBLL.authorization;
using EAristotelDomain.model.material;

namespace EAristotelBLL.services.impl
{
    internal class MaterialService : IMaterialService
    {
        private readonly PermissionManager _permissionManager;

        public MaterialService(PermissionManager permissionManager)
        {
            _permissionManager = permissionManager;
        }

        public void CreateMaterial(int repositoryId, Material material, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.MATERIAL_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                var leRepo = c.LeRepositoryRepo.Fetch(repositoryId);
                material.Repository = leRepo;
                c.MaterialRepo.Add(material);
            });
        }

        public Material GetMaterial(int materialId, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => c.MaterialRepo.Fetch(materialId));
        }

        public IList<Material> GetMaterials(int repositoryId, BllContext context = null)
        {
            return
                ExecInContextService.ExecInContext(context,
                    c => c.MaterialRepo.FetchMany(e => e.Repository.Id.Equals(repositoryId)).ToList());
        }

        public IList<Material> Search(string query, BllContext context = null)
        {
            return ExecInContextService.ExecInContext(context, c => SearchUtilities.SimpleSearch(query, c.MaterialRepo));
        }

        public IList<Material> GetAllMaterials()
        {
            return ExecInContextService.ExecInContext(null, c => c.MaterialRepo.FetchAll().ToList());
        }

        public void RemoveMaterial(int materialId, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.MATERIAL_CRUD);
            ExecInContextService.ExecInContext(context, c =>
            {
                var material = c.MaterialRepo.Fetch(materialId);
                c.MaterialRepo.Remove(material);
            });
        }

        public void UpdateMaterial(Material material, string token, BllContext context = null)
        {
            _permissionManager.GetPermissionBundle(token).CheckPermission(Permission.MATERIAL_CRUD);
            ExecInContextService.ExecInContext(context, c => c.MaterialRepo.Update(material));
        }
    }
}