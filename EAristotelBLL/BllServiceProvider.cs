﻿using EAristotelBLL.authorization;
using EAristotelBLL.services;
using EAristotelBLL.services.impl;

namespace EAristotelBLL
{
    public class BllServiceProvider : IBllServiceProvider
    {
        private readonly PermissionManager _permissionManager = new PermissionManager();

        private AccountService _accountService;
        public IAccountService AccountService => _accountService ?? (_accountService = new AccountService(_permissionManager));

        private ExamService _examService;
        public IExamService ExamService => _examService ?? (_examService = new ExamService(_permissionManager));

        private ExerciseService _exerciseService;
        public IExerciseService ExerciseService => _exerciseService ?? (_exerciseService = new ExerciseService(_permissionManager));

        private LeRepositoryService _leRepositoryService;

        public ILeRepositoryService LeRepositoryService
            => _leRepositoryService ?? (_leRepositoryService = new LeRepositoryService(_permissionManager, this));

        private MaterialService _materialService;
        public IMaterialService MaterialService => _materialService ?? (_materialService = new MaterialService(_permissionManager));
    }
}