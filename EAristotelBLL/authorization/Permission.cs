﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.authorization
{
    /// <summary>
    /// Permissions for actions that are not accessible out-of-the-box
    /// </summary>
    public enum Permission
    {
        REPOSITORY_CRUD, EXAM_CRUD,
        EXERCISE_DELETE, SOLUTION_DELETE, CHOOSE_SOLUTION,
        MATERIAL_CRUD
    }
}
