﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.util.exceptions;

namespace EAristotelBLL.authorization
{
    public class PermissionBundle
    {
        private readonly HashSet<Permission> _permissionsSet = new HashSet<Permission>();

        public PermissionBundle(PermissionBundle basePermissionBundle, params Permission[] permissions) : this(permissions)
        {
            foreach (Permission permission in basePermissionBundle._permissionsSet)
            {
                _permissionsSet.Add(permission);
            }
        }

        public PermissionBundle(params Permission[] permissions)
        {
            foreach (Permission permission in permissions)
            {
                _permissionsSet.Add(permission);
            }
        }

        public bool HasPermission(Permission permission)
        {
            return _permissionsSet.Contains(permission);
        }

        /// <summary>
        /// Throws AristotelPermissionException if this PermissionBundle does not contain given permission
        /// </summary>
        /// <param name="permission"></param>
        public void CheckPermission(Permission permission)
        {
            if (!HasPermission(permission))
            {
                //throw new AristotelPermissionException(permission.ToString());
                throw new AristotelBaseException(permission.ToString());
            }
        }
    }
}
