﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDomain.util.exceptions;

namespace EAristotelBLL.authorization
{
    internal class PermissionManager
    {
        private readonly Dictionary<string, PermissionBundle> _activeTokens = new Dictionary<string, PermissionBundle>();
        private static int aut = 0;

        private string GenerateToken()
        {
            var token = "random" + aut++;
            return token;
        }

        /// <summary>
        /// Saves given PermissionBundle and returns generated token
        /// </summary>
        /// <returns>Generated token</returns>
        internal string SavePermissionBundle(PermissionBundle permissionBundle)
        {
            var token = GenerateToken();
            _activeTokens.Add(token, permissionBundle);
            return token;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token">string token</param>
        /// <returns></returns>
        internal PermissionBundle GetPermissionBundle(string token)
        {
            PermissionBundle permissionBundle;
            if (_activeTokens.TryGetValue(token, out permissionBundle))
            {
                return permissionBundle;
            }
            throw new AristotelBaseException("Invalid token");
        }
    }
}