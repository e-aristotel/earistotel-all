﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAristotelBLL.authorization
{
    public static class PermissionBundleFactory
    {
        public static PermissionBundle CreateAdminPermissionBundle()
        {
            return new PermissionBundle(CreateDefaultPermissionBundle(), 
                Permission.REPOSITORY_CRUD, 
                Permission.EXAM_CRUD, 
                Permission.MATERIAL_CRUD, 
                Permission.CHOOSE_SOLUTION);
        }

        public static PermissionBundle CreateDefaultPermissionBundle()
        {
            return new PermissionBundle();
        }
    }
}
