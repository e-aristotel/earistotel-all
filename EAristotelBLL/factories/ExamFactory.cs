﻿using System.Collections.Generic;
using EAristotelDomain.model.exam;

namespace EAristotelBLL.factories
{
    public class ExamFactory
    {
        public static Exam CreateExam(string name, List<ExamTask> examTasks)
        {
            Exam exam= new Exam
            {
                Name = name,
                ExamTasks = examTasks
            };
            examTasks.ForEach(examTask=>examTask.Exam=exam);

            return exam;
        }

        public static ExamTask CreateExamTask(string content, List<ExamTaskSolution> offeredSolutions,
            ExamTaskSolution correctSolution)
        {
            ExamTask examTask = new ExamTask()
            {
                Content = content,
                OfferedSolutions = offeredSolutions,
                CorrectSolution = correctSolution
            };

            offeredSolutions.ForEach(offeredSolution => offeredSolution.ExamTask = examTask);
            correctSolution.ExamTask = examTask;

            return examTask;
        }

        public static ExamTaskSolution CreateExamTaskSolution(string result)
        {
            return new ExamTaskSolution()
            {
                Result = result
            };
        }
    }
}