﻿using System.Collections.Generic;
using EAristotelDomain.model.exercise;

namespace EAristotelBLL.factories
{
    public class ExerciseFactory
    {
        public static Exercise CreateExercise(string name, string content, List<string> tags = null,
            List<ExerciseSolution> solutions = null, ExerciseSolution bestSolution = null)
        {
            Exercise exercise = new Exercise
            {
                Name = name,
                Content = content,
                BestSolution = bestSolution
            };
            if (solutions != null)
            {
                exercise.Solutions = solutions;
                solutions.ForEach(solution => solution.Exercise = exercise);
            }

            if (tags != null)
            {
                exercise.Tags = tags;    
            }
            return exercise;
        }

        public static ExerciseSolution CreateExerciseSolution(string result, string stepByStep = null)
        {
            return new ExerciseSolution
            {
                Result = result,
                StepByStep = stepByStep
            };
        }
    }
}