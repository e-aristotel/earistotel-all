﻿using EAristotelDomain.model.material;

namespace EAristotelBLL.factories
{
    public class MaterialFactory
    {
        public static Material CreateMaterial(string name, string author, string content)
        {
            return new Material {Name = name, Author = author, Content = content};
        }
    }
}
