﻿using EAristotelDomain.model.repository;

namespace EAristotelBLL.factories
{
    public class LeRepositoryFactory
    {
        public static LeRepository CreateRepository(string name, LeRepository parent = null)
        {
            var leRepository = new LeRepository
            {
                Name = name,
                ParentRepository = parent
            };
            parent?.ChildrenRepositories.Add(leRepository);
            return leRepository;
        }
    }
}
