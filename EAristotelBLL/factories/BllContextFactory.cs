﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAristotelDAL.repositories.impl;

namespace EAristotelBLL.services
{
    public static class BllContextFactory
    {
        public static BllContext CreateContext()
        {
            return new BllContext(new DalContext());
        }
    }
}
