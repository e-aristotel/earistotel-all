﻿using System;
using EAristotelDAL.repositories;
using EAristotelDomain.repositories;
using EAristotelDomain.util;

namespace EAristotelBLL
{
    public class BllContext: IDisposable
    {
        private readonly IDalContext _context;

        public BllContext(IDalContext context)
        {
            _context = context;
        }

        //vazno je da je ovo sve internal
        internal IUnitOfWork UnitOfWork => _context.UnitOfWork;
        internal IAdminRepo AdminRepo => _context.AdminRepo;
        internal IExamRepo ExamRepo => _context.ExamRepo;
        internal IExerciseRepo ExerciseRepo => _context.ExerciseRepo;
        internal IExerciseSolutionRepo ExerciseSolutionRepo => _context.ExerciseSolutionRepo;
        internal ILeRepositoryRepo LeRepositoryRepo => _context.LeRepositoryRepo;
        internal IMaterialRepo MaterialRepo => _context.MaterialRepo;

        public void Dispose()
        {
            UnitOfWork?.Dispose();
        }
    }
}
